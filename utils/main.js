import Router from "next/router"

// import request from "request"
export function postData(url = "", data = {}) {
  return fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  }).then(response => response.json()) // parses JSON response into native Javascript objects
}

export function loginCheck() {
  if (typeof window == "object") {
    if (window.localStorage.getItem("usertoken") === null) {
      Router.push("/login")
    } else {
      const userTk = window.localStorage.getItem("usertoken")
      postData(
        "https://api.mrezak.ir/update.php?action=tokencheck&token=" + userTk
      ).then(data => {
        if (data == "good") {
          // console.log("latest")
          this.setState({
            logged: 0
          })
        } else {
          console.log(data)
          // Router.push("/login")
        }
      })
    }
  }
}

export function logout() {
  if (typeof window == "object") {
    if (window.localStorage.getItem("mallreporttoken") === null) {
      Router.push("/login")
    } else {
      window.localStorage.removeItem("mallreporttoken")
      Router.push("/login")
    }
  }
}
