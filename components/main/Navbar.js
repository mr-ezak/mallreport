import styled from "styled-components"
import Link from "next/link"
import * as Icons from "./Icons"
import { logout } from "../../utils/main"

export const Navbar = props => {
  return (
    <NavWrapper right={props.right}>
      {props.per == 1 ? (
        <RawNavbar>
          <ul>
            <Link href='/'>
              <li class='line'>
                <Icons.DashIcon />
                <a>پیشخوان</a>
              </li>
            </Link>
            <Link href='/addShop'>
              <li>
                <Icons.PlusIcon />
                <a>افزودن فروشگاه</a>
              </li>
            </Link>
            <Link href='/shopsManage'>
              <li class='line'>
                <Icons.GroupIcon />
                <a>مدیریت فروشگاه ها</a>
              </li>
            </Link>
            <Link href='/addSingleSum'>
              <li>
                <Icons.BookIcon />
                <a>ثبت مجموع فروش</a>
              </li>
            </Link>
            <Link href='/addSingleCut'>
              <li>
                <Icons.BookIcon />
                <a>کسر از موجودی</a>
              </li>
            </Link>
            <Link href='/shopSumManage'>
              <li>
                <Icons.BookIcon />
                <a>مدیریت مجموع فروش</a>
              </li>
            </Link>
            <Link href='/sumReport'>
              <li class='line'>
                <Icons.BookIcon />
                <a>گزارش گیری</a>
              </li>
            </Link>
            <Link href='addUser'>
              <li>
                <Icons.AddUserIcon />
                <a>افزودن کاربر</a>
              </li>
            </Link>
            <Link href='/usersManage'>
              <li class='line'>
                <Icons.UsersIcon />
                <a>مدیریت کاربران</a>
              </li>
            </Link>
            <li onClick={() => logout()}>
              <Icons.SignoutIcon />
              <a>خروج</a>
            </li>
          </ul>
        </RawNavbar>
      ) : (
        <div>
          {props.per == 2 ? (
            <RawNavbar>
              <ul>
                <Link href='/'>
                  <li class='line'>
                    <Icons.DashIcon />
                    <a>پیشخوان</a>
                  </li>
                </Link>
                <Link href='/addSingleSum'>
                  <li>
                    <Icons.BookIcon />
                    <a>ثبت مجموع فروش</a>
                  </li>
                </Link>
                <Link href='/addSingleCut'>
                  <li>
                    <Icons.BookIcon />
                    <a>کسر از موجودی</a>
                  </li>
                </Link>
                <Link href='/shopSumManage'>
                  <li>
                    <Icons.BookIcon />
                    <a>مدیریت مجموع فروش</a>
                  </li>
                </Link>
                <li onClick={() => logout()}>
                  <Icons.SignoutIcon />
                  <a>خروج</a>
                </li>
              </ul>
            </RawNavbar>
          ) : (
            <RawNavbar>
              <ul>
                <Link href='/'>
                  <li class='line'>
                    <Icons.DashIcon />
                    <a>پیشخوان</a>
                  </li>
                </Link>
                <Link href='/sumReport'>
                  <li class='line'>
                    <Icons.BookIcon />
                    <a>گزارش گیری</a>
                  </li>
                </Link>
                <li onClick={() => logout()}>
                  <Icons.SignoutIcon />
                  <a>خروج</a>
                </li>
              </ul>
            </RawNavbar>
          )}
        </div>
      )}
    </NavWrapper>
  )
}

export const NavBlank = props => {
  return <MainNavBlank display={props.display} {...props} />
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const NavWrapper = styled.div`
  width: 200px;
  height: 100%;
  position: fixed;
  top: 50px;
  bottom: 0;
  right: ${p => (p.right ? p.right : 0)}px;
  z-index: 2;
  background: #fff;
  font-family: BYekan;
  transition: all 350ms;
`

const MainNavBlank = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.7);
  z-index: 1;
  top: 50px;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 350ms;
`

/* - - - RawNav Components - - - */

const RawNavbar = styled.div`
  width: 100%;
  height: auto;
  direction: rtl;
  ul {
    width: 100%;
    height: auto;
    padding-right: 0px;
    list-style: none;
    overflow: hidden;
    margin: 0;
  }
  li[class="line"] {
    border-bottom: 1px solid #999;
  }
  li {
    width: 100%;
    height: 30px;
    color: #333;
    line-height: 30px;
    padding-bottom: 10px;
    cursor: pointer;
    svg {
      width: 20px;
      height: 20px;
      display: block;
      margin: 10px auto;
      padding-right: 10px;
      float: right;
    }
    a {
      width: 150px;
      height: 40px;
      display: block;
      float: right;
      line-height: 40px;
      font-size: 14px;
      padding-right: 10px;
      color: #333;
    }
  }
  li:hover {
    background: #43abe6;
    a {
      color: #fff;
    }
  }
`
