import React, { Component } from "react"
import styled from "styled-components"
import { LetterIcon, BarsIcon } from "./Icons"
import { Navbar, NavBlank } from "./Navbar"
import { postData } from "../../utils/main"

export class Header extends Component {
  state = {
    navstat: "closed",
    navright: -230,
    display: "none",
    per: ""
  }

  handlePerCheck = () => {
    if (typeof window == "object") {
      const userTk = window.localStorage.getItem("mallreporttoken")
      postData(
        "https://ser01.mrezak.ir/update.php?action=percheck&token=" + userTk
      ).then(data => {
        if (data[0] == "good") {
          this.setState({
            per: data[1]
          })
        } else {
          console.log(data)
        }
      })
    }
  }

  handleNavClick = () => {
    const navstatus = this.state.navstat
    if (navstatus == "open") {
      this.setState({
        navstat: "closed",
        navright: -230,
        display: "none"
      })
    } else if (navstatus == "closed") {
      this.setState({
        navstat: "open",
        navright: 0,
        display: "block"
      })
    }
  }

  handleNavCloser = () => {
    const navstatus = this.state.navstat
    if (navstatus == "open") {
      this.setState({
        navstat: "closed",
        navright: -230,
        display: "none"
      })
    }
  }

  componentDidMount() {
    this.handlePerCheck()
  }

  render = () => (
    <>
      <HeaderWrapper>
        <UperHeader>
          <HeaderBars onClick={this.handleNavClick}>
            <BarsIcon />
          </HeaderBars>
          {/* <HeaderNotification>
            <LetterIcon />
          </HeaderNotification> */}
        </UperHeader>
      </HeaderWrapper>
      <Navbar right={this.state.navright} per={this.state.per} />
      <NavBlank onClick={this.handleNavCloser} display={this.state.display} />
    </>
  )
}

/* - - - Page Components - - - */

const HeaderWrapper = styled.div`
  width: 100%;
  height: 50px;
  background: #fff;
  text-align: center;
  overflow: hidden;
  div[class="input-container"] {
    width: 200px;
    height: 40px;
    float: right;
    margin-right: 20px;
    margin-top: 20px;
    position: relative;
  }
  p[class="header-title"] {
    display: block;
    margin: 0px auto;
    width: 100px;
    height: 36px;
    line-height: 36px;
    color: #fff;
  }
`
const UperHeader = styled.div`
  width: 100%;
  height: 40px;
  overflow: hidden;
`

// const HeaderNotification = styled.div`
//   float: left;
//   width: 40px;
//   height: 40px;

//   svg {
//     display: block;
//     margin: 15px auto;
//     margin-left: 10px;
//   }
// `
const HeaderBars = styled.div`
  float: right;
  width: 30px;
  height: 50px;
  margin-right: 10px;
  cursor: pointer;
  svg {
    display: block;
    margin: 10px auto;
  }
`
