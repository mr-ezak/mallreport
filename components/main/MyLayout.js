import styled from "styled-components"
import { Header } from "./Header"

const Layout = props => (
  <Wrapper>
    <Header />
    <Content>
      {/* <img bg="unique" src="../../static/images/circle.png" /> */}
      {props.children}
    </Content>
  </Wrapper>
)

export default Layout

const Wrapper = styled.div`
  overflow: hidden;
`
const Content = styled.div`
  width: 100%;
  height: auto;
  top: 50px;
  bottom: 0px;
  overflow: auto;
  position: absolute;
  background: #fff;
  z-index: -2;
`
