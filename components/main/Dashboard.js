import React, { Component } from "react"
import styled from "styled-components"
import { postData } from "../../utils/main"
import {
  SmallLoadingIcon,
  MoneyIcon,
  BasketIcon,
  MultipleUsersIcon
} from "./Icons"

export class Dashboard extends Component {
  state = {
    info: "",
    color1: "#43ab92",
    color2: "#ff991f",
    orangeBg: "../static/images/DashBox_Orange.jpg",
    greenBg: "../static/images/DashBox_Green.jpg"
  }

  handleDashInfo = () => {
    postData("https://ser01.mrezak.ir/update.php?action=dashboard").then(
      data => {
        this.setState({
          info: data
        })
      }
    )
  }

  componentDidMount() {
    this.handleDashInfo()
  }

  render = () => (
    <>
      {this.state.info == "" ? (
        <DashWrapper>
          <DashDataLoading>
            <SmallLoadingIcon />
          </DashDataLoading>
          <DashDataLoading>
            <SmallLoadingIcon />
          </DashDataLoading>
          <DashDataLoading>
            <SmallLoadingIcon />
          </DashDataLoading>
          <DashDataLoading>
            <SmallLoadingIcon />
          </DashDataLoading>
        </DashWrapper>
      ) : (
        <DashWrapper>
          {/* - - - Dashboard Boxes - - - */}

          <DashBox bg={this.state.orangeBg}>
            <div class='DashBoxpic'>
              <MoneyIcon />
            </div>
            <DashBoxTitle color={this.state.color1}>
              <span>فروش کل</span>
            </DashBoxTitle>
            <p>{this.state.info[0]} تومن</p>
          </DashBox>

          <DashBox bg={this.state.greenBg}>
            <div class='DashBoxpic'>
              <MoneyIcon />
            </div>
            <DashBoxTitle color={this.state.color2}>
              <span>برداشت ها</span>
            </DashBoxTitle>
            <p>{this.state.info[1]} تومن</p>
          </DashBox>

          <DashBox bg={this.state.orangeBg}>
            <div class='DashBoxpic'>
              <BasketIcon />
            </div>
            <DashBoxTitle color={this.state.color1}>
              <span>فروشگاه ها</span>
            </DashBoxTitle>
            <p>{this.state.info[2]} عدد</p>
          </DashBox>

          <DashBox bg={this.state.greenBg}>
            <div class='DashBoxpic'>
              <MultipleUsersIcon />
            </div>
            <DashBoxTitle color={this.state.color2}>
              <span>کاربران</span>
            </DashBoxTitle>
            <p>{this.state.info[3]} نفر</p>
          </DashBox>
        </DashWrapper>
      )}
    </>
  )
}

const DashWrapper = styled.div`
  width: 100%;
  height: 100%;
  background: #fff;
  border-top: 2px solid #333;
  box-sizing: border-box;
  direction: rtl;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`
const DashDataLoading = styled.div`
  width: 300px;
  height: 200px;
  background: #ccc;
  margin-top: 100px;
  margin-right: 30px;
  border-radius: 10px;
  svg {
    display: block;
    margin: 10px auto;
  }
`
const DashBox = styled.div`
  width: 300px;
  height: 200px;
  background: url("${p => p.bg}") no-repeat;
  margin-top: 50px;
  margin-right: 20px;
  direction: rtl;
  position: relative;
  border-radius: 10px;
  box-shadow: 0px 0px 5px #999;
  p {
    width: 100%;
    text-align: center;
    margin-top: 40px;
    font-size: 18px;
    color: #333;
  }
  div[class="DashBoxpic"] {
    display: none;
    width: 60px;
    height: 60px;
    background: #fff;
    box-shadow: 0px 0px 7px #666;
    border-radius: 30px;
    position: absolute;
    top: 5px;
    right: 10px;
    svg {
      display: block;
      margin-top: 12px;
      margin-left: 10px;
    }
  }
`
const DashBoxTitle = styled.div`
  width: 100%;
  height: 70px;
  // background: ${p => (p.color ? p.color : "#333")};
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  overflow: hidden;
  span {
    width: 100%;
    text-align: right;
    direction: rtl;
    display: block;
    height: 30px;
    padding-right: 70px;
    margin-top: 28px;
    color: #333;
    font-size: 16px;
    font-weight: bold;
  }
`
