import styled from "styled-components"
import Link from "next/link"
import * as Icons from "./Icons"

export const MiniNavbar = props => {
  return (
    <NavWrapper right={props.right}>
      <RawNavbar>
        <ul>
          <Link href='/'>
            <li class='line'>
              <Icons.DashIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.PlusIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.GroupIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.BookIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.SettingIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.AddUserIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.UsersIcon />
            </li>
          </Link>
          <Link href='#'>
            <li class='line'>
              <Icons.SignoutIcon />
            </li>
          </Link>
        </ul>
      </RawNavbar>
    </NavWrapper>
  )
}

export const MiniNavBlank = props => {
  return <MainNavBlank display={props.display} {...props} />
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const NavWrapper = styled.div`
  width: 40px;
  height: 100%;
  position: fixed;
  top: 50px;
  bottom: 0;
  right: ${p => (p.right ? p.right : 0)}px;
  z-index: 2;
  background: #fff;
  font-family: BYekan;
  transition: all 350ms;
`

const MainNavBlank = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  z-index: 1;
  top: 0;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 350ms;
`

/* - - - RawNav Components - - - */

const RawNavbar = styled.div`
  width: 100%;
  height: auto;
  direction: rtl;
  ul {
    width: 100%;
    height: auto;
    padding-right: 0px;
    list-style: none;
    overflow: hidden;
    margin: 0;
  }
  li[class="line"] {
    border-bottom: 1px solid #999;
  }
  li {
    width: 100%;
    height: 30px;
    color: #333;
    line-height: 30px;
    padding-bottom: 10px;
    cursor: pointer;
    svg {
      width: 20px;
      height: 20px;
      display: block;
      margin: 10px auto;
      padding-right: 10px;
      float: right;
    }
  }
  li:hover {
    background: #43abe6;
  }
`
