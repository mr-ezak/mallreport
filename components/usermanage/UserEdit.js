import React, { Component } from "react"
import styled from "styled-components"
import Router from "next/router"
import { postData } from "../../utils/main"

export class UserEdit extends Component {
  state = {
    id: "",
    fullname: "",
    username: "",
    password: ""
  }

  handleUserEdit = () => {
    /* - - - Set The Shop data - - - */
    if (this.state.fullname == "") {
      this.setState({
        fullname: this.props.fullname
      })
    } else if (this.state.username == "") {
      this.setState({
        username: this.props.username
      })
    } else {
      if (this.state.password == "") {
        this.setState({
          password: this.props.password
        })
      } else {
        let userData = {
          id: this.props.id,
          fullname: this.state.fullname,
          username: this.state.username,
          password: this.state.password
        }
        postData(
          "https://ser01.mrezak.ir/update.php?action=useredit",
          userData
        ).then(data => {
          if (data[0] == "done") {
            window.location.reload()
          } else {
            console.log(data)
          }
        })
      }
    }
    /* - - - Set The Shop manager - - - */
  }

  render = () => (
    <ShopEditWrapper top={this.props.top}>
      <ShopEditForm>
        <ShopEditItemContainer>
          <input type='hidden' name='id' value={this.props.id} />
          <input
            type='text'
            name='fullname'
            placeholder={this.props.fullname}
            onChange={() => this.setState({ fullname: event.target.value })}
          />
        </ShopEditItemContainer>
        <ShopEditItemContainer>
          <input
            type='text'
            name='username'
            placeholder={this.props.username}
            onChange={() => this.setState({ username: event.target.value })}
          />
        </ShopEditItemContainer>
        <ShopEditItemContainer>
          <input
            type='text'
            name='password'
            placeholder='رمز عبور جدید'
            onChange={() => this.setState({ password: event.target.value })}
          />
        </ShopEditItemContainer>
        <ShopEditBTN onClick={() => this.handleUserEdit()}>
          به روز رسانی
        </ShopEditBTN>
      </ShopEditForm>
    </ShopEditWrapper>
  )
}

export class UserEditBlank extends Component {
  render = () => (
    <MainShopEditBlank display={this.props.display} {...this.props} />
  )
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const ShopEditWrapper = styled.div`
  width: 350px;
  height: 300px;
  position: fixed;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  top: ${p => (p.top ? p.top : 100)}px;
  z-index: 2;
  background: #f9f8eb;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #999;
  transition: all 500ms;
`

const MainShopEditBlank = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  z-index: 1;
  top: 0;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 500ms;
`
const ShopEditForm = styled.form`
  width: 100%;
  height: auto;
  overflow: hidden;
  span {
    display: block;
    width: 100%;
    height: 30px;
  }
`
const ShopEditItemContainer = styled.div`
  width: 90%;
  height: 40px;
  border-bottom: 1px solid #ff991f;
  margin: 20px auto;
  svg {
    width: 20px;
    height: 20px;
    float: right;
    margin-left: 5px;
    margin-top: 10px;
  }
  input {
    width: 87%;
    padding-right: 5px;
    direction: rtl;
    margin-top: 5px;
    height: 30px;
    float: right;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
  }
  input::placeholder {
    color: #67b0af;
    opacity: 1;
    font-size: 12px;
    font-family: BYekan;
  }
`
const ShopEditBTN = styled.div`
  width: 150px;
  height: 40px;
  display: block;
  background: #ff991f;
  border-radius: 10px;
  border: medium none;
  text-align: center;
  line-height: 40px;
  color: #fff;
  font-family: BYekan;
  margin: 40px auto;
  cursor: pointer;
`
