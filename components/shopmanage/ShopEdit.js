import React, { Component } from "react"
import styled from "styled-components"
import Router from "next/router"
import { postData } from "../../utils/main"

export class ShopEdit extends Component {
  state = {
    id: "",
    name: "",
    manager: ""
  }

  handleShopEdit = () => {
    /* - - - Set The Shop data - - - */
    if (this.state.name == "") {
      this.setState({
        name: this.props.name
      })
    } else {
      if (this.state.manager == "") {
        this.setState({
          manager: this.props.manager
        })
      } else {
        let shopData = {
          id: this.props.id,
          name: this.state.name,
          manager: this.state.manager
        }
        postData(
          "https://ser01.mrezak.ir/update.php?action=shopedit",
          shopData
        ).then(data => {
          if (data[0] == "done") {
            window.location.reload()
          } else {
            console.log(data)
          }
        })
      }
    }
    /* - - - Set The Shop manager - - - */
  }

  render = () => (
    <ShopEditWrapper top={this.props.top}>
      <ShopEditForm>
        <ShopEditItemContainer>
          <input type='hidden' name='name' value={this.props.id} />
          <input
            type='text'
            name='name'
            placeholder={this.props.name}
            onChange={() => this.setState({ name: event.target.value })}
          />
        </ShopEditItemContainer>
        <ShopEditItemContainer>
          <input
            type='text'
            name='manager'
            placeholder={this.props.manager}
            onChange={() => this.setState({ manager: event.target.value })}
          />
        </ShopEditItemContainer>
        <ShopEditBTN onClick={() => this.handleShopEdit()}>
          به روز رسانی
        </ShopEditBTN>
      </ShopEditForm>
    </ShopEditWrapper>
  )
}

export class ShopEditBlank extends Component {
  render = () => (
    <MainShopEditBlank display={this.props.display} {...this.props} />
  )
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const ShopEditWrapper = styled.div`
  width: 350px;
  height: 250px;
  position: fixed;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  top: ${p => (p.top ? p.top : 100)}px;
  z-index: 2;
  background: #f9f8eb;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #999;
  transition: all 500ms;
`

const MainShopEditBlank = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  z-index: 1;
  top: 0;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 500ms;
`
const ShopEditForm = styled.form`
  width: 100%;
  height: auto;
  overflow: hidden;
  span {
    display: block;
    width: 100%;
    height: 30px;
  }
`
const ShopEditItemContainer = styled.div`
  width: 90%;
  height: 40px;
  border-bottom: 1px solid #ff991f;
  margin: 20px auto;
  svg {
    width: 20px;
    height: 20px;
    float: right;
    margin-left: 5px;
    margin-top: 10px;
  }
  input {
    width: 87%;
    padding-right: 5px;
    direction: rtl;
    margin-top: 5px;
    height: 30px;
    float: right;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
  }
  input::placeholder {
    color: #67b0af;
    opacity: 1;
    font-size: 12px;
    font-family: BYekan;
  }
`
const ShopEditBTN = styled.div`
  width: 150px;
  height: 40px;
  display: block;
  background: #ff991f;
  border-radius: 10px;
  border: medium none;
  text-align: center;
  line-height: 40px;
  color: #fff;
  font-family: BYekan;
  margin: 40px auto;
  cursor: pointer;
`
