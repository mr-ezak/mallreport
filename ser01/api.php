<?php
	include('config.inc.php');
	include('jdf.php');
	$action = $_GET['action'];
	
	/* - - - Set the Headers for the permissions - - - */
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json; charset=utf-8');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, token ');
	header("HTTP/1.1 200 OK");
	
	/* - - - Recieve the action and give the right respond back - - - */


	
	if($action == "keyrequest") {
		// $info = $db->query("INSERT INTO `services`(`id`, `name`, `cat`, `subCat`, `des`, `created`, `modified`) VALUES ('0','0','0','0','0','0','0')");
		echo json_encode("boom");
	}

	if($action == "profile") {
		$id = $_SESSION['vahid-user-id'];
		$info = $db->query("SELECT * FROM `admins` WHERE `id` = '$id'")->fetch_assoc();
		echo json_encode($info);
	}

	if($action == "geteventcode") {
		function codeCreator() {
			Global $db;
			$code = "me-".date("y").mt_rand(1, 100000).date("m").date("d");
			$info = $db->query("SELECT * FROM `EventOffer` WHERE `offerCode` = '$code'");
			if($info->num_rows == 0){
				echo json_encode($code);
			}else{
				codeCreator();
			}
		}
		codeCreator();
	}

	if($action == "eventmanage") {
		$info = $db->query("SELECT * FROM `EventOffer` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "getofferuser") {
		$info = $db->query("SELECT `id`, `username` FROM `users` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	// if($action == "geteventcode") {
	// 	function codeCreator() {
	// 		Global $db;
	// 		$code = "me-".date("y").mt_rand(1, 100000).date("m").date("d");
	// 		$info = $db->query("SELECT * FROM `EventOffer` WHERE `offerCode` = '$code'");
	// 		if($info->num_rows == 0){
	// 			echo json_encode($code);
	// 		}else{
	// 			codeCreator();
	// 		}
	// 	}
	// 	codeCreator();
	// }

	if($action == "getoffercode") {
		function offerCodeCreator() {
			Global $db;
			$code = "mo-".date("y").mt_rand(1, 100000).date("m").date("d");
			$info = $db->query("SELECT * FROM `Offers` WHERE `offer_code` = '$code'");
			if($info->num_rows == 0){
				echo json_encode($code);
			}else{
				offerCodeCreator();
			}
		}
		offerCodeCreator();
	}

	if($action == "offermanage") {
		$info = $db->query("SELECT `off`.*, `us`.`username` AS `user_name`  FROM `Offers` AS `off`
							LEFT JOIN `users` AS `us` ON `off`.`user_id` = `us`.`id`
							WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}
	
	if($action == "basketsmanage") {
		$info = $db->query("SELECT `bsk`.* , `usr`.`username` AS `us_name` FROM `baskets` AS `bsk`
							LEFT JOIN `users` AS `usr` ON `bsk`.`basket_user_id` = `usr`.`id`
							WHERE `bsk`.`basket_archived` = '0'");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			$get['created'] = implode("-",gregorian_to_jalali((explode("-",$get['created']))[0],(explode("-",$get['created']))[1],(explode("-",$get['created']))[2]));
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "bskpaycheck") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `baskets` WHERE `id`='$id'")->fetch_assoc();

		if($info['basket_active'] == 1){
			echo json_encode("yes");
		}else{
			echo json_encode("no");
		}
	}

	if($action == "bsksent") {
		$id = $_GET['id'];
		$info = $db->query("UPDATE `baskets` SET `basket_sent`='1' WHERE `id`='$id'");
		echo json_encode("done");
	}

	if($action == "basketspromanage") {
		$id = $_GET['id'];
		$info = $db->query("SELECT `bskp`.* , `bsks`.`basket_price` AS `bsks_price` , `bsks`.`basket_address` AS `bsks_addr` , `bsks`.`id` AS `bsks_id` , `bsks`.`basket_sent` AS `bsks_sent` FROM `basketspro` AS `bskp`
							LEFT JOIN `baskets` AS `bsks` ON `bskp`.`basket_id` = `bsks`.`basket_id`
							WHERE `bskp`.`basket_id` = (SELECT `basket_id` FROM `baskets` WHERE `id`='$id')");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "promanage") {
		$info = $db->query("SELECT `prds`.* , `cat`.`title` AS `catname` FROM `prd` AS `prds`
							LEFT JOIN `categories` AS `cat` ON `prds`.`ProCat` = `cat`.`id`
							WHERE 1 LIMIT 50");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "promanagesearch") {
		$selected = $_GET['selected'];
		$keyword = $_GET['keyword'];
		if($selected == "barcode") {
			$info = $db->query("SELECT `prd`.* , `cat`.`title` AS `catname` FROM `prd` AS `prd`
								LEFT JOIN `categories` AS `cat` ON `prd`.`ProCat` = `cat`.`id`
								WHERE `prd`.`ProId` = '$keyword'");
			$arr = array();
			while($get = $info->fetch_assoc()) {
				array_push($arr, $get);
			}
			echo json_encode(array('info'=>$arr));
		}else if($selected == "name") {
			$info = $db->query("SELECT `prd`.* , `cat`.`title` AS `catname` FROM `prd` AS `prd` 
								LEFT JOIN `categories` AS `cat` ON `prd`.`ProCat` = `cat`.`id`
								WHERE `prd`.`ProName` LIKE '%$keyword%'");
			$arr = array();
			while($get = $info->fetch_assoc()) {
				array_push($arr, $get);
			}
			echo json_encode(array('info'=>$arr));
		}else if($selected == "off") {
			$info = $db->query("SELECT `prd`.* , `cat`.`title` AS `catname` FROM `prd` AS `prd` 
								LEFT JOIN `categories` AS `cat` ON `prd`.`ProCat` = `cat`.`id`
								WHERE `prd`.`ProOffActive` = '1'");
			$arr = array();
			while($get = $info->fetch_assoc()) {
				array_push($arr, $get);
			}
			echo json_encode(array('info'=>$arr));
		}
	}

	if($action == "getproinfo") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `prd` WHERE `id` = '$id'");
		$barcode = $db->query("SELECT `ProId` FROM `prd` WHERE `id` = '$id'")->fetch_assoc();
		$path = "./uploads/".$barcode['ProId'];
		$picnames = glob("$path/*.jpg");
		
		
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}

		for($i = 0; $i < 3; $i++){
			if(isset($picnames[$i])) {
				$picaddr = $picnames[$i];
				array_unshift($arr, $picaddr);
			}else{
				$empty = "";
				array_unshift($arr, $empty);	
			}
		}

		
		echo json_encode(array('info'=>$arr));
	}

	if($action == "getbanners") {
		$info = $db->query("SELECT * FROM `banners` WHERE 1");
		// $path = "./banners";
		// $picnames = glob("$path/*.jpg");
		
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		
		echo json_encode(array('info'=>$arr));
	}

	if($action == "propicremove") {
		$barcode = $_GET['barcode'];
		$link = $_GET['link'];

		$linkcon = explode("/",$link);
		$picname = end($linkcon);

		$path = "./uploads/".$barcode."/".$picname;
		
		if(!unlink($path)) {
			echo json_encode("error");
		}else{
			echo json_encode("done");
		}
		
	}

	if($action == "bannerremove") {
		$id = $_GET['id'];
		$link = $_GET['link'];

		if(!unlink($link)) {
			echo json_encode("error");
		}else{
			$date = date("Y-m-d");
			$db->query("UPDATE `banners` SET `link`='',`modified`='$date' WHERE `id` = '$id'");
			echo json_encode("done");
		}
	}
	
	if($action == "supplymanage") {
		$info = $db->query("SELECT * FROM `supplier` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "getsuppinfo") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `supplier` WHERE `id`='$id'")->fetch_assoc();
		echo json_encode($info);
	}

	if($action == "getuserinfo") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `admins` WHERE `id` = '$id'");

		$arr = array();
		while($get = $info->fetch_assoc()) {
			unset($get['admin_password']);
			unset($get['created']);
			unset($get['modified']);
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "getdatesandprices") {
		/* - - - Baskets Dates - - - */
		$info1 = $db->query("SELECT `created` FROM `baskets` WHERE 1 ORDER BY `created` ASC LIMIT 1")->fetch_assoc();
		$info2 = $db->query("SELECT `created` FROM `baskets` WHERE 1 ORDER BY `created` DESC LIMIT 1")->fetch_assoc();
		$date1 = implode("-",gregorian_to_jalali((explode("-",$info1['created']))[0],(explode("-",$info1['created']))[1],(explode("-",$info1['created']))[2]));
		$date2 = implode("-",gregorian_to_jalali((explode("-",$info2['created']))[0],(explode("-",$info2['created']))[1],(explode("-",$info2['created']))[2]));
		
		/* - - - Baskets Prices - - - */
		$info3 = $db->query("SELECT `basket_price` FROM `baskets` WHERE `basket_price`=(SELECT MIN(`basket_price`) FROM `baskets`)")->fetch_assoc();
		$info4 = $db->query("SELECT `basket_price` FROM `baskets` WHERE `basket_price`=(SELECT MAX(`basket_price`) FROM `baskets`)")->fetch_assoc();
		$price1 = $info3['basket_price'];
		$price2 = $info4['basket_price'];
		
		/* - - - Products Dates - - - */
		$info5 = $db->query("SELECT `created` FROM `baskets` WHERE 1 ORDER BY `created` ASC LIMIT 1")->fetch_assoc();
		$info6 = $db->query("SELECT `created` FROM `baskets` WHERE 1 ORDER BY `created` DESC LIMIT 1")->fetch_assoc();
		$date3 = implode("-",gregorian_to_jalali((explode("-",$info5['created']))[0],(explode("-",$info5['created']))[1],(explode("-",$info5['created']))[2]));
		$date4 = implode("-",gregorian_to_jalali((explode("-",$info6['created']))[0],(explode("-",$info6['created']))[1],(explode("-",$info6['created']))[2]));
		
		/* - - - Events Dates - - - */
		$info7 = $db->query("SELECT `created` FROM `eventoffer` WHERE 1 ORDER BY `created` ASC LIMIT 1")->fetch_assoc();
		$info8 = $db->query("SELECT `created` FROM `eventoffer` WHERE 1 ORDER BY `created` DESC LIMIT 1")->fetch_assoc();
		$date5 = implode("-",gregorian_to_jalali((explode("-",$info7['created']))[0],(explode("-",$info7['created']))[1],(explode("-",$info7['created']))[2]));
		$date6 = implode("-",gregorian_to_jalali((explode("-",$info8['created']))[0],(explode("-",$info8['created']))[1],(explode("-",$info8['created']))[2]));

		$arr = array($date1, $date2, $price1, $price2, $date3, $date4, $date5, $date6);
		echo json_encode(array('info'=>$arr));
	}

	/* - - - Report Codes - - - */

	if($action == "basketsearch") {

		$fromd = implode("-",jalali_to_gregorian((explode("-",$_GET['fromd']))[0],(explode("-",$_GET['fromd']))[1],(explode("-",$_GET['fromd']))[2]));
		$tod = implode("-",jalali_to_gregorian((explode("-",$_GET['tod']))[0],(explode("-",$_GET['tod']))[1],(explode("-",$_GET['tod']))[2]));
		$fromp = $_GET['fromp'];
		$top = $_GET['top'];
		
		$info = $db->query("SELECT `bsk`.* , `usr`.`username` AS `us_name` FROM `baskets` AS `bsk`
							LEFT JOIN `users` AS `usr` ON `bsk`.`basket_user_id` = `usr`.`id`
							WHERE `bsk`.`basket_sent` = '1' AND (`basket_price` >= '$fromp' AND `basket_price` <= '$top') AND (`bsk`.created >= '$fromd' AND `bsk`.`created` <= '$tod')");
				
		$arr = array();
		$sumprice = 0;
		$sumbasket = 0;
		while($get = $info->fetch_assoc()) {
			$get['created'] = implode("-",gregorian_to_jalali((explode("-",$get['created']))[0],(explode("-",$get['created']))[1],(explode("-",$get['created']))[2]));
			$sumprice = $sumprice + $get['basket_price'];
			$sumbasket = $sumbasket + 1;
			array_push($arr, $get);
		}
		array_unshift($arr, $sumbasket);
		array_unshift($arr, $sumprice);
		echo json_encode(array('info'=>$arr));
	}
	
	if($action == "basketprosearch") {

		$fromd = implode("-",jalali_to_gregorian((explode("-",$_GET['fromd']))[0],(explode("-",$_GET['fromd']))[1],(explode("-",$_GET['fromd']))[2]));
		$tod = implode("-",jalali_to_gregorian((explode("-",$_GET['tod']))[0],(explode("-",$_GET['tod']))[1],(explode("-",$_GET['tod']))[2]));
		$procode = $_GET['procode'];
		
		$info = $db->query("SELECT `bskp`.* , `bsks`.`created` AS `bsks_created` , `usr`.`username` AS `us_name` FROM `basketspro` AS `bskp`
							LEFT JOIN `baskets` AS `bsks` ON `bskp`.`basket_id` = `bsks`.`basket_id`
							RIGHT JOIN `users` AS `usr` ON `bsks`.`basket_user_id` = `usr`.`id`
							WHERE (`bskp`.`pro_id`='$procode' AND `bsks`.`basket_sent` = '1') AND (`bsks`.`created` >= '$fromd' AND `bsks`.`created` <= '$tod')");

		$procount = 0;
		$sumprice = 0;
		$sumcount = 0;
		$arr = array();
		while($get = $info->fetch_assoc()) {
			$get['bsks_created'] = implode("-",gregorian_to_jalali((explode("-",$get['bsks_created']))[0],(explode("-",$get['bsks_created']))[1],(explode("-",$get['bsks_created']))[2]));
			$sumprice = $sumprice + $get['pro_final_price'];
			$sumcount = $sumcount + $get['pro_count'];
			$procount = $procount + 1;
			array_push($arr, $get);
		}
		array_unshift($arr, $sumcount);
		array_unshift($arr, $sumprice);
		array_unshift($arr, $procount);
		echo json_encode(array('info'=>$arr));
	}
	
	if($action == "eventsearch") {

		$fromd = implode("-",jalali_to_gregorian((explode("-",$_GET['fromd']))[0],(explode("-",$_GET['fromd']))[1],(explode("-",$_GET['fromd']))[2]));
		$tod = implode("-",jalali_to_gregorian((explode("-",$_GET['tod']))[0],(explode("-",$_GET['tod']))[1],(explode("-",$_GET['tod']))[2]));
		
		$info = $db->query("SELECT * FROM `eventoffer`
							WHERE `created` >= '$fromd' AND `created` <= '$tod'");
				
		$sumcount = 0;
		$arr = array();
		while($get = $info->fetch_assoc()) {
			$get['created'] = implode("-",gregorian_to_jalali((explode("-",$get['created']))[0],(explode("-",$get['created']))[1],(explode("-",$get['created']))[2]));
			$sumcount = $sumcount + 1;
			array_push($arr, $get);
		}
		array_unshift($arr, $sumcount);
		echo json_encode(array('info'=>$arr));
	}

	if($action == "usermanage") {
		$info = $db->query("SELECT * FROM `admins` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "useractive") {
		$id = $_GET['id'];
		$modified = date("Y-m-d");
		$db->query("UPDATE `admins` SET `admin_active`='1',`modified`='$modified' WHERE `id`='$id'");
		echo json_encode("done");
	}
	
	if($action == "userdeactivate") {
		$id = $_GET['id'];
		$curid = $_SESSION['vahid-user-id'];
		if($id == $curid) {
			echo json_encode("same");
		}else{
			$modified = date("Y-m-d");
			$db->query("UPDATE `admins` SET `admin_active`='0',`modified`='$modified' WHERE `id`='$id'");
			echo json_encode("done");
		}
	}

	if($action == "getuseredit") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `admins` WHERE `id` = '$id'");
		while($get = $info->fetch_assoc()) {
			$myData = new StdClass;
			$myData->id = $get['id'];;
			$myData->name = $get['name'];;
			$myData->username = $get['username'];
			echo json_encode($myData);
		}
	}

	if($action == "getcat") {
		$info = $db->query("SELECT * FROM `categories` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "getsupp") {
		$info = $db->query("SELECT * FROM `supplier` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "geteditcat") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `categories` WHERE `id` = '$id'");
		while($get = $info->fetch_assoc()) {
			$myData = new StdClass;
			$myData->id = $get['id'];;
			$myData->title = $get['title'];;
			$myData->dis = $get['dis'];
			echo json_encode($myData);
		}
	}
	
	if($action == "catmanageget") {
		$info = $db->query("SELECT * FROM `categories` WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}
	
	if($action == "getsubcat") {
		$id = $_GET['id'];
		$info = $db->query("SELECT * FROM `subcat` WHERE `catId`='$id'");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "getsubcatinfo") {
		$id = $_GET['id'];
		$info = $db->query("SELECT `scats`.* , `cats`.`title` AS `cats_name` FROM `subcat` AS `scats`
							LEFT JOIN `categories` AS `cats` ON `scats`.`catId` = `cats`.`id`
							WHERE `scats`.`id` = '$id'");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}
	
	if($action == "subcatmanageget") {
		$info = $db->query("SELECT `scat`.* , `cat`.`title` as `cat_name` FROM `subcat` as `scat`
							LEFT JOIN `categories` as cat ON `scat`.`catId` = `cat`.`id`
							WHERE 1");
		$arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}

	if($action == "ProCodeGen") {
		$barcode = $_GET['barcode'];
		if($barcode == ""){
			echo json_encode("fuck");
		}else{
			$getcode = $db->query("SELECT * FROM `prd` WHERE `ProId` = '$barcode'");
			if($getcode->num_rows == 0) {
				$_SESSION[$_SESSION['vahid-user-lg']] = $barcode;
				echo json_encode("done");
			}else{
				echo json_encode("fuck");
			}
		}
	}

	if($action == "procodefinder") {
		$barcode = $_GET['barcode'];
		$_SESSION[$_SESSION['vahid-user-lg']] = $barcode;
		echo json_encode("BCD Done");
	}
	
	if($action == "cleansession") {
		if(isset($_SESSION[$_SESSION['vahid-user-lg']])){
			unset($_SESSION[$_SESSION['vahid-user-lg']]);
			echo json_encode("Cleaned");
		}else{
			echo json_encode("Doesn't Exist");
		}
	}
