<?php
	include('config.inc.php');
	include('jdf.php');
	$action = $_GET['action'];
	

    // 	Behnam COdE
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json; charset=utf-8');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, token ');
	header("HTTP/1.1 200 OK");



    /* - - - Functions - - - */
    
    function getTheSingleSum($shopId,$cutPrice) {
        #Get the shop's data from DB and put inside of an array.
        global $db;
        $info = $db->query("SELECT * FROM `shopsSingleSum` WHERE `shopId`='$shopId'");
        $arr = array();
    	while($get = $info->fetch_assoc()) {
    		array_push($arr, $get);
    	}
    	
    	#Get the prices out of the shops data array.
    	$arr2 = array();
    	$arr3 = array();
    	foreach($arr as $value) {
    	    if($value['type'] == "p") {
    	        array_push($arr2, $value['price']);    
    	    }else if($value['type'] == "n") {
    	        array_push($arr3, $value['price']);
    	    }
    	    
    	}
    	
    	#Separate the negative from positive.
    	$positive = array_sum($arr2);
    	$negative = array_sum($arr3);
    
    	$result = ($positive - $negative) - $cutPrice;
        
    	if($result > 0) {
    	    return true;
    	   
    	}else if($result < 0){
    	    return false;
    	}
    }



	/* - - - LOGIN - - - */
	

	if($action == "login") {
	    $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $username = $data->username;
	    $password = $data->password;
	    if($password == "" || $username == "") {
	        echo json_encode(array('emp'));
	    }else{
	        $pass = md5($password);
    		$user = $db->query("SELECT * FROM `users` WHERE `username`='$username' and `password`='$pass'");
    		if($user->num_rows == 1){
    		    $token = md5(rand(1000000, 9000000));
    		    $modified = date("Y-m-d");
    		    $dt = $user->fetch_assoc();
    		    $userid = $dt['id'];
    		    $userpermission = $dt['permission'];
    		    $db->query("UPDATE `users` SET `token`='$token', `modified`='$modified' WHERE `id` = '$userid'");
    			echo json_encode(array('logged', $token, $userpermission));
    		}else if($user->num_rows == 0){
    			echo json_encode(array('notexist'));
    		}else{
    			echo json_encode(array('fuck'));
    		}   
	    }
	}
	
	if($action == "dashboard") {
	    // Create the Sum report
	    $sum = $db->query("SELECT `price` FROM `shopsSingleSum` WHERE `type`='p'");
	    $sumarr = array();
		while($get = $sum->fetch_assoc()) {
			array_push($sumarr, $get['price']);
		}
		
		$sumresult = array_sum($sumarr);
		
		// Create the Cut report
		$cut = $db->query("SELECT `price` FROM `shopsSingleSum` WHERE `type`='n'");
	    $cutarr = array();
		while($get = $cut->fetch_assoc()) {
			array_push($cutarr, $get['price']);
		}
		$cutresult = array_sum($cutarr);
		
		// Create the Shops count report
		$shops = $db->query("SELECT * FROM `shops` WHERE 1")->num_rows;
		
		
		// Create the Users count report
		$users = $db->query("SELECT * FROM `users` WHERE 1")->num_rows;
		
		echo json_encode(array($sumresult, $cutresult, $shops, $users));
		
	}
	
	if($action == "signup") {
	    $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $email = $data->email;
	    $password = $data->password;
	    $repassword = $data->repassword;
	    if($email == "" || $password == "" || $repassword == "") {
	        echo json_encode(array('empty'));
	    }else if($password != $repassword) {
	        echo json_encode(array('notmactch'));
	    }else{
	        $data = $db->query("SELECT * FROM `users` WHERE `email`='$email'");
	        if($data->num_rows == 1){
	            echo json_encode(array('notagain'));
	        }else{
	            $pass = md5($password);
    	        $token = md5(rand(1000000, 9000000));
    	        $created = date("Y-m-d h:i:s");
    	        $modified = date("Y-m-d");
    	        $db->query("INSERT INTO `users`(`id`, `fullName`, `email`, `password`, `mode`, `token`, `created`, `modified`) VALUES (NULL,'0','$email','$pass','box','$token','$created','$modified')");
    	        echo json_encode(array('done', $token));
	        }
	    }
	}
	
	if($action == "tokencheck") {
	    $token = $_GET['token'];
	    $data = $db->query("SELECT * FROM `users` WHERE `token`='$token'");
	    if($data->num_rows == 1) {
	        echo json_encode('good');
	    }else{
	        echo json_encode('fuck');
	       //if($dt = $data->fetch_assoc()) {
        //     echo json_encode();    
	       //}else{
	       // echo json_encode("no");
	       //}
	    }
	}
	
	if($action == "percheck") {
	    $token = $_GET['token'];
	    $data = $db->query("SELECT * FROM `users` WHERE `token`='$token'")->fetch_assoc();
	    $per = $data['permission'];
	    echo json_encode(array("good",$per));
	}
	
	/* - - - Shops Management - - - */
	
	if($action == "addshop") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $name = $data->name;
	    $manager = $data->manager;
	    if($name == "" || $manager == "") {
	        echo json_encode(array('empty'));
	    } else {
	        $created = date("Y-m-d");
	        $db->query("INSERT INTO `shops`(`id`, `name`, `manager`, `created`, `modified`) VALUES (NULL,'$name','$manager','$created','$created')");
	        echo json_encode(array('done'));
	    }
    }
    
    if($action == "shopslistreq") {
        $info = $db->query("SELECT * FROM `shops` WHERE 1");
        $arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
    }
    
    if($action == "shopremove") {
        if($id = $_GET['id']) {
            $db->query("DELETE FROM `shopsSingleSum` WHERE `shopId`='$id'");
            $db->query("DELETE FROM `shops` WHERE `id`='$id'");
            echo json_encode(array('done'));
        }else{
            echo json_encode(array('fuck'));
        }
    }
    
    if($action == "shopeditinfo") {
        if($id = $_GET['id']) {
            $info = $db->query("SELECT * FROM `shops` WHERE `id`='$id'")->fetch_assoc();
            echo json_encode($info);
        }
    }
    
    if($action == "shopedit") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $id = $data->id;
	    $name = $data->name;
	    $manager = $data->manager;
	    if($id == "" || $name == "" || $manager == "") {
	        echo json_encode(array($id, $name, $manager));
	    }else{
	        $modified = date("Y-m-d");
    	    $db->query("UPDATE `shops` SET `name`='$name',`manager`='$manager',`modified`='$modified' WHERE `id`='$id'");
    	    echo json_encode(array('done'));
	    }
    }
    
    
    if($action == "addshopsum") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $userToken = $data->userToken;
	    $shopId = $data->shopId;
	    $sumPrice = $data->sumPrice;
	    $fromDate = $data->fromDate;
	    $toDate = $data->toDate;
	    
	    $fromd = explode("/",$fromDate);
	    $from = jalali_to_gregorian(intval($fromd[0]),intval($fromd[1]),intval($fromd[2]),'-');
	    
	    $tod = explode("/",$toDate);
	    $to = jalali_to_gregorian(intval($tod[0]),intval($tod[1]),intval($tod[2]),'-');
	    
	    if($shopId == "" || $sumPrice == "" || $fromDate == "" || $toDate == "") {
	        echo json_encode(array("emp"));
	    }else{
	        $info = $db->query("SELECT * FROM `users` WHERE `token`='$userToken'")->fetch_assoc();
	        $userId = $info['id'];
	        $created = date("Y-m-d");
	        $db->query("INSERT INTO `shopsSingleSum`(`id`, `userId`, `shopId`, `price`, `type`, `startDate`, `endDate`, `created`, `modified`) VALUES (NULL,'$userId','$shopId','$sumPrice','p','$from','$to','$created','$created')");
	        echo json_encode(array("done"));
	    }
    }
    
    
    if($action == "addshopcut") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $userToken = $data->userToken;
	    $shopId = $data->shopId;
	    $cutPrice = $data->cutPrice;
	    $fromDate = $data->fromDate;
	    $toDate = $data->toDate;
	    
	    
	    $fromd = explode("/",$fromDate);
	    $from = jalali_to_gregorian(intval($fromd[0]),intval($fromd[1]),intval($fromd[2]),'-');
	    
	    $tod = explode("/",$toDate);
	    $to = jalali_to_gregorian(intval($tod[0]),intval($tod[1]),intval($tod[2]),'-');
	    
	    
	    
	    if($shopId == "" || $cutPrice == "") {
	        echo json_encode(array("emp"));
	    }else{
	        $info = $db->query("SELECT * FROM `users` WHERE `token`='$userToken'")->fetch_assoc();
	        $userId = $info['id'];
	        
	        if(getTheSingleSum($shopId,$cutPrice) == true) {
	            $created = date("Y-m-d");
    	        $db->query("INSERT INTO `shopsSingleSum`(`id`, `userId`, `shopId`, `price`, `type`, `startDate`, `endDate`, `created`, `modified`) VALUES (NULL,'$userId','$shopId','$cutPrice','n','$from','$to','$created','$created')");
    	        echo json_encode(array("done"));
	        }else if(getTheSingleSum($shopId,$cutPrice) == false) {
	            echo json_encode(array("fuck"));
	        }
	        
	    }
    }
    
    if($action == "shopsumreq") {
        $info = $db->query("SELECT `shss`.*, `sh`.`name`  FROM `shopsSingleSum` AS `shss`
                            LEFT JOIN `shops` AS `sh` ON `shss`.`shopId` = `sh`.`id`
                            WHERE 1 ORDER BY `shss`.`created` DESC");
        $arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
    }
    
    /* - - - User Management - - - */
    
    if($action == "adduser") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $fullname = $data->fullname;
	    $username = $data->username;
	    $password = $data->password;
	    $permission = $data->permission;
	    if($fullname == "" || $username == "" || $password == "" || $permission == "") {
	        echo json_encode(array("emp"));
	    }else{
	        $user = $db->query("SELECT * FROM `users` WHERE `username`='$username'");
            if($user->num_rows == 0) {
                $pass = md5($password);
    	        $created = date("Y-m-d");
    	        $token = 0;
    	        $db->query("INSERT INTO `users`(`id`, `fullName`, `username`, `password`, `permission`, `token`, `created`, `modified`) VALUES (NULL,'$fullname','$username','$pass','$permission','$token','$created','$created')");
    	        echo json_encode(array("done"));
            }else{
                echo json_encode(array("exist"));
            }
	    }
    }
    
    if($action == "userslistreq") {
        $info = $db->query("SELECT * FROM `users` WHERE 1");
        $arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
    }
    
    if($action == "userremove") {
        if($id = $_GET['id']) {
            $db->query("DELETE FROM `users` WHERE `id`='$id'"); 
            echo json_encode(array('done'));
        }else{
            echo json_encode(array('fuck'));
        }
    }
    
    if($action == "usereditinfo") {
        if($id = $_GET['id']) {
            $info = $db->query("SELECT * FROM `users` WHERE `id`='$id'")->fetch_assoc();
            echo json_encode($info);
        }
    }
    
    if($action == "useredit") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $id = $data->id;
	    $fullname = $data->fullname;
	    $username = $data->username;
	    $password = $data->password;
	    if($id == "" || $fullname == "" || $username == "") {
	        echo json_encode(array("emp"));
	    }else{
	        if($password == "") {
	            $modified = date("Y-m-d");
        	    $db->query("UPDATE `users` SET `fullName`='$fullname',`username`='$username',`modified`='$modified' WHERE `id`='$id'");
        	    echo json_encode(array('done'));
	        }else if($password != "") {
	            $modified = date("Y-m-d");
	            $pass = md5($password);
        	    $db->query("UPDATE `users` SET `fullName`='$fullname',`username`='$username',`password`='$pass',`modified`='$modified' WHERE `id`='$id'");
        	    echo json_encode(array('done'));
	        }
	    }
    }
    
    /* - - - Report process - - - */
    
    if($action == "sumreport") {
        $json = file_get_contents('php://input');
	    $data = json_decode($json);
	    $shopId = $data->shopId;
	    $fromDate = $data->fromDate;
	    $toDate = $data->toDate;
	    
	    if($shopId == "0") {

	        $reports = array();
            $shopsarr = array();
            $data = $db->query("SELECT `id` FROM `shops` WHERE 1");
            while($get = $data->fetch_assoc()) {
    		    array_push($shopsarr, $get['id']);
        	}
        	foreach($shopsarr as $value) {
        	    $id = $value;
        	    
        	    #Get the records from DB
        	    $name = $db->query("SELECT `name` FROM `shops` WHERE `id`='$id'")->fetch_assoc();
        	    $manager = $db->query("SELECT `manager` FROM `shops` WHERE `id`='$id'")->fetch_assoc();
        	    
        	    $fromd = explode("/",$fromDate);
        	    $from = jalali_to_gregorian(intval($fromd[0]),intval($fromd[1]),intval($fromd[2]),'-');
        	    
        	    $tod = explode("/",$toDate);
        	    $to = jalali_to_gregorian(intval($tod[0]),intval($tod[1]),intval($tod[2]),'-');
        	    
	            $info = $db->query("SELECT * FROM `shopsSingleSum` WHERE `shopId`='$id' AND (`startDate`>='$from' AND `endDate`<='$to')");
                $arr = array();
                    
                while($get = $info->fetch_assoc()) {
        		    array_push($arr, $get);
            	}
            	
            	#Get the prices out of the shops data array.
            	$arr2 = array();
            	$arr3 = array();
            	foreach($arr as $value) {
            	    if($value['type'] == "p") {
            	        array_push($arr2, $value['price']);    
            	    }else if($value['type'] == "n") {
            	        array_push($arr3, $value['price']);
            	    }
            	    
            	}
	            #Calculate the result
	            $positive = array_sum($arr2);
            	$negative = array_sum($arr3);
            	$result = $positive - $negative;
                
                // $index = "shop".$id;
                // array_push($reports, $result);
                
                $all[] = $result;
                $reports[] = ['id' => $id, 'name' => $name['name'], 'manager' => $manager['manager'], 'result' => $result];
        	       
        	}
            $taken = gregorian_to_jalali(date("Y"),date("m"),date("d"),"-");
	        $allsum = array_sum($all);         
	        echo json_encode(array($reports, $allsum, $taken));
	    }else{
	        
	        $name = $db->query("SELECT `name` FROM `shops` WHERE `id`='$shopId'")->fetch_assoc();
    	    $manager = $db->query("SELECT `manager` FROM `shops` WHERE `id`='$shopId'")->fetch_assoc();
    	    
    	    $fromd = explode("/",$fromDate);
    	    $from = jalali_to_gregorian(intval($fromd[0]),intval($fromd[1]),intval($fromd[2]),'-');
    	    
    	    $tod = explode("/",$toDate);
    	    $to = jalali_to_gregorian(intval($tod[0]),intval($tod[1]),intval($tod[2]),'-');
        	    
	        $info = $db->query("SELECT * FROM `shopsSingleSum` WHERE `shopId`='$shopId' AND (`startDate`>='$from' AND `endDate`<='$to')");
	        
	        
	        $arr = array();
                    
            while($get = $info->fetch_assoc()) {
    		    array_push($arr, $get);
        	}
        	
        	#Get the prices out of the shops data array.
        	$arr2 = array();
        	$arr3 = array();
        	foreach($arr as $value) {
        	    if($value['type'] == "p") {
        	        array_push($arr2, $value['price']);    
        	    }else if($value['type'] == "n") {
        	        array_push($arr3, $value['price']);
        	    }
        	    
        	}
            #Calculate the result
            $positive = array_sum($arr2);
        	$negative = array_sum($arr3);
        	$result = $positive - $negative;
        	
        	$reports[] = ['id' => $id, 'name' => $name['name'], 'manager' => $manager['manager'], 'result' => $result];
        	$taken = gregorian_to_jalali(date("Y"),date("m"),date("d"),"-");
	        
	        echo json_encode(array($reports, $result, $taken));
	    }
    }

    /* - - - Services Management - - - */
    
    if($action == "servlistreq") {
        $info = $db->query("SELECT * FROM `services` WHERE 1");
        $arr = array();
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
    }
    
	if($action == "servicesingle") {
        $id = $_GET['id'];
    	$info = $db->query("SELECT * FROM `services` WHERE `id`='$id'")->fetch_assoc();
    	echo json_encode($info);
	}
	
	if($action == "servreq") {
	    $usertoken = $_GET['token'];
        $servid = $_GET['id'];
        $userinfo = $db->query("SELECT * FROM `users` WHERE `token`='$usertoken'")->fetch_assoc();
        $userid = $userinfo['id'];
        $created = date("Y-m-d h:i:s");
    	$info = $db->query("INSERT INTO `serviceReqs`(`id`, `serviceId`, `userId`, `created`) VALUES (NULL,'$servid','$userid','$created')");
    	echo json_encode("done");
	}

	/* - - - User Services History Management - - - */

	if($action == "historyreq"){
		$usertoken = $_GET['token'];
		$userInfo = $db->query("SELECT * FROM `users` WHERE `token`='$usertoken'")->fetch_assoc();
		$userId = $userInfo[id];
	    $arr = array();
	    $info = $db->query("SELECT `servreq`.*, `servs`.`des` AS `servs_des` FROM `serviceReqs` AS `servreq`
	                        LEFT JOIN `users` AS `us` ON `servreq`.`userId` = `us`.`id`
	                        LEFT JOIN `services` AS `servs` ON `servreq`.`serviceId` = `servs`.`id`
	                        WHERE `userId`='$userId'");
		while($get = $info->fetch_assoc()) {
			array_push($arr, $get);
		}
		echo json_encode(array('info'=>$arr));
	}
	
	if($action == "historysingle") {
        $id = $_GET['id'];
    	$info = $db->query("SELECT `servreq`.*, `servs`.`des`, `servs`.`code` FROM `serviceReqs` AS `servreq`
    	                    LEFT JOIN `services` AS `servs` ON `servreq`.`serviceId` = `servs`.`id`
    	                    WHERE `servreq`.`id`='$id'")->fetch_assoc();
    	echo json_encode($info);
	}
	
	/* - - - User Setup Management - - - */
	
	if($action == "getkeytoken") {
	    $usertoken = $_GET['token'];
	    $userinfo = $db->query("SELECT * FROM `users` WHERE `token`='$usertoken'")->fetch_assoc();
        $keytoken = $userinfo['keyToken'];
        echo json_encode($keytoken);
	}

	/* - - - User Manegement - - - */

	if($action == "getuserdata") {
		$usertoken = $_GET['token'];
	    $userinfo = $db->query("SELECT * FROM `users` WHERE `token`='$usertoken'")->fetch_assoc();
        echo json_encode($userinfo);
	}

	if($action == "userupdate") {
		$json = file_get_contents('php://input');
		$data = json_decode($json);
		$modified = date("Y-m-d h:i:s");
		$db->query("UPDATE `users` SET `name`='$data->name',`family`='$data->family',`mcode`='$data->mcode',`phoneNumber`='$data->phonenumber',`homeNumber`='$data->homenumber',`address`='$data->address',`modified`='$modified' WHERE `id`='$data->userid'");
		echo json_encode("done");
	}
    
	/* - - - Product Create - - - */

	if($action == "productcreate"){

		/* - - - Check The Admin For Being Active - - - */
		if(isset($_SESSION['vahid-user-id'])){
			$id = $_SESSION['vahid-user-id'];
			$info = $db->query("SELECT `admin_active` FROM `admins` WHERE `id` = '$id'")->fetch_assoc();
			if($info['admin_active'] == 1){
				/* - - - Check The Barcode - - - */
				if(isset($_SESSION[$_SESSION['vahid-user-lg']])) {

					/* - - - Check The Existence Of Pictures - - - */
					$storeFolder = './uploads/'.$_SESSION[$_SESSION['vahid-user-lg']];
					if(!file_exists($storeFolder)) {
						echo "upload";
					}else{

						/* - - - Recive The Posted Informations - - - */
						foreach($_POST as $k=>$v)
						{
							if(!is_array($v))
								$$k = $v;
						}

						/* - - - Prepare Some Of Information - - - */
						$barcode = $_SESSION[$_SESSION['vahid-user-lg']];
						if($poff == ""){$off = 0;$poff = 0;}else{$off = 1;}
						$creator = $_SESSION['vahid-user-lg'];
						$created = date("Y-m-d");

						/* - - - Check The Product Unit & Send The Information in the DB - - - */
						if($pradio == "number") {
							$unit = 1;
							$db->query("INSERT INTO `prd`(`id`, `ProId`, `ProName`, `ProSpecs`, `ProBuyPrice`, `ProPrice`, `ProOff`, `ProOffActive`, `ProCat`, `ProSubCat`, `ProUnit`, `ProCount`, `ProLikes`, `ProVisits`, `ProSupplier`, `ProCreator`, `Created`, `Modified`) VALUES (NULL,'$barcode','$pname','$pspecs','$pbuyprice','$pprice','$poff','$off','$pcat','$psubcat','$unit','$pcountnum','0','0','$mpcSupp','$creator','$created','$created')");
							echo "done";
						}elseif($pradio == "kilo") {
							$unit = 2;
							$db->query("INSERT INTO `prd`(`id`, `ProId`, `ProName`, `ProSpecs`, `ProBuyPrice`, `ProPrice`, `ProOff`, `ProOffActive`, `ProCat`, `ProSubCat`, `ProUnit`, `ProCount`, `ProLikes`, `ProVisits`, `ProSupplier`, `ProCreator`, `Created`, `Modified`) VALUES (NULL,'$barcode','$pname','$pspecs','$pbuyprice','$pprice','$poff','$off','$pcat','$psubcat','$unit','$pcountkilo','0','0','$mpcSupp','$creator','$created','$created')");
							echo "done";
						}elseif($pradio == "hour") {
							$unit = 3;
							if($pcounthour1 == 0 || $pcounthour1 == "") {
								$pcounthour1 == 0;
							}
							if($pcounthour2 == 0 || $pcounthour2 == "") {
								$pcounthour2 == 0;
							}
							if($pcounthour3 == 0 || $pcounthour3 == "") {
								$pcounthour3 == 0;
							}
							if($pcounthour4 == 0 || $pcounthour4 == "") {
								$pcounthour4 == 0;
							}
							
							if($pcounthour2 >= $pcounthour1 && $pcounthour4 >= $pcounthour3) {
								$prhour = $pcounthour1.",".$pcounthour2.",".$pcounthour3.",".$pcounthour4;
								$db->query("INSERT INTO `prd`(`id`, `ProId`, `ProName`, `ProSpecs`, `ProBuyPrice`, `ProPrice`, `ProOff`, `ProOffActive`, `ProCat`, `ProSubCat`, `ProUnit`, `ProCount`, `ProLikes`, `ProVisits`, `ProSupplier`, `ProCreator`, `Created`, `Modified`) VALUES (NULL,'$barcode','$pname','$pspecs','$pbuyprice','$pprice','$poff','$off','$pcat','$psubcat','$unit','$prhour','0','0','$mpcSupp','$creator','$created','$created')");
								echo "done";
							}else{
								echo "time";
							}
						}
					}
				}else{
					echo "barcode";
				}
			}else if($info['admin_active'] == 0){
				echo "active";
			}	
		}
	}

	if($action == "prodelete") {
		if($id = $_GET['id']){
			$barcode = $db->query("SELECT `ProId` FROM `prd` WHERE `id` = '$id'")->fetch_assoc();
			$path = "./uploads/".$barcode['ProId'];
			array_map('unlink', glob("$path/*.jpg"));
			rmdir($path);
			$db->query("DELETE FROM `prd` WHERE `id` = '$id'");
		}
	}

	if($action == "productupdate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		$info = $db->query("SELECT `ProId` FROM `prd` WHERE `id` = '$pid'")->fetch_assoc();
		if($info['ProId'] != $pcode) {
			$count = $db->query("SELECT `ProId` FROM `prd` WHERE `ProId` = '$pcode'")->num_rows;
			if($count == 0) {
				$path = "./uploads/".$info['ProId'];
				$newpath = "./uploads/".$pcode;
				rename($path,$newpath);
				
				if($poff == "" || $poff == 0){$off = 0;$poff = 0;}else{$off = 1;}
				$modified = date("Y-m-d");
				if($pradio == "number") {
					$unit = 1;
					$db->query("UPDATE `prd` SET `ProId`='$pcode',`ProName`='$pname',`ProSpecs`='$pspecs',`ProBuyPrice`='$pbuyprice',`ProPrice`='$pprice',`ProOff`='$poff',`ProOffActive`='$off',`ProCat`='$pcat',`ProSubCat`='$psubcat',`ProUnit`='$unit',`ProCount`='$pcountnum',`ProSupplier`='$psupp',`Modified`='$modified' WHERE `id` = '$pid'");
					echo "done";
				}elseif($pradio == "kilo") {
					$unit = 2;
					$db->query("UPDATE `prd` SET `ProId`='$pcode',`ProName`='$pname',`ProSpecs`='$pspecs',`ProBuyPrice`='$pbuyprice',`ProPrice`='$pprice',`ProOff`='$poff',`ProOffActive`='$off',`ProCat`='$pcat',`ProSubCat`='$psubcat',`ProUnit`='$unit',`ProCount`='$pcountkilo',`ProSupplier`='$psupp',`Modified`='$modified' WHERE `id` = '$pid'");
					echo "done";
				}elseif($pradio == "hour") {
					$unit = 3;
					$prhour = $pcounthour1.",".$pcounthour2.",".$pcounthour3.",".$pcounthour4;
					$db->query("UPDATE `prd` SET `ProId`='$pcode',`ProName`='$pname',`ProSpecs`='$pspecs',`ProBuyPrice`='$pbuyprice',`ProPrice`='$pprice',`ProOff`='$poff',`ProOffActive`='$off',`ProCat`='$pcat',`ProSubCat`='$psubcat',`ProUnit`='$unit',`ProCount`='$prhour',`ProSupplier`='$psupp',`Modified`='$modified' WHERE `id` = '$pid'");
					echo "done";
				}
			}else{
				echo "taken";
			}
		}else{
			if($poff == "" || $poff == 0){$off = 0;$poff = 0;}else{$off = 1;}
			$modified = date("Y-m-d");
			if($pradio == "number") {
				$unit = 1;
				$db->query("UPDATE `prd` SET `ProName`='$pname',`ProSpecs`='$pspecs',`ProBuyPrice`='$pbuyprice',`ProPrice`='$pprice',`ProOff`='$poff',`ProOffActive`='$off',`ProCat`='$pcat',`ProSubCat`='$psubcat',`ProUnit`='$unit',`ProCount`='$pcountnum',`ProSupplier`='$psupp',`Modified`='$modified' WHERE `id` = '$pid'");
				echo "done";
			}elseif($pradio == "kilo") {
				$unit = 2;
				$db->query("UPDATE `prd` SET `ProName`='$pname',`ProSpecs`='$pspecs',`ProBuyPrice`='$pbuyprice',`ProPrice`='$pprice',`ProOff`='$poff',`ProOffActive`='$off',`ProCat`='$pcat',`ProSubCat`='$psubcat',`ProUnit`='$unit',`ProCount`='$pcountkilo',`ProSupplier`='$psupp',`Modified`='$modified' WHERE `id` = '$pid'");
				echo "done";
			}elseif($pradio == "hour") {
				$unit = 3;
				$prhour = $pcounthour1.",".$pcounthour2.",".$pcounthour3.",".$pcounthour4;
				$db->query("UPDATE `prd` SET `ProName`='$pname',`ProSpecs`='$pspecs',`ProBuyPrice`='$pbuyprice',`ProPrice`='$pprice',`ProOff`='$poff',`ProOffActive`='$off',`ProCat`='$pcat',`ProSubCat`='$psubcat',`ProUnit`='$unit',`ProCount`='$prhour',`ProSupplier`='$psupp',`Modified`='$modified' WHERE `id` = '$pid'");
				echo "done";
			}
		}
	}

	if($action == "prooffchange") {
		$type = $_GET['type'];
		$pers = $_GET['pers'];
		if($type == "offactive") {
			foreach($_POST as $k=>$v)
			{
				if(!is_array($v))
					$$k = $v;
			}
			$count = count($v);
			$modified = date("Y-m-d"); 
			for($i = 0; $i < $count; $i++) {
				if($v[$i] != "") {
					$barcode = $v[$i];
					$db->query("UPDATE `prd` SET `ProOff`='$pers',`ProOffActive`='1',`Modified`='$modified' WHERE `ProId` = '$barcode' AND `ProOffActive` = '0'");
				}
			}
			echo "done";
		}else if($type == "offedit") {
			foreach($_POST as $k=>$v)
			{
				if(!is_array($v))
					$$k = $v;
			}
			$count = count($v);
			$modified = date("Y-m-d"); 
			for($i = 0; $i < $count; $i++) {
				if($v[$i] != "") {
					$barcode = $v[$i];
					$db->query("UPDATE `prd` SET `ProOff`='$pers',`Modified`='$modified' WHERE `ProId` = '$barcode' AND `ProOffActive` = '1'");
				}
			}
			echo "done";
		}
	}

	if($action == "prooffdeactive") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		$count = count($v);
		$modified = date("Y-m-d"); 
		for($i = 0; $i < $count; $i++) {
			if($v[$i] != "") {
				$barcode = $v[$i];
				$db->query("UPDATE `prd` SET `ProOff`='0',`ProOffActive`='0',`Modified`='$modified' WHERE `ProId` = '$barcode'");
			}
		}
		echo "done";
	}

	/* - - - Event Manage - - - */

	if($action == "eventcreate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		if($v == "") {
			echo "empty";
		}else{
			$date = date("Y-m-d");
			$db->query("INSERT INTO `EventOffer`(`id`, `offerName`, `offerDes`, `offerCode`, `offerPercentage`, `offerActive`, `created`) VALUES (NULL,'$eventname','$eventdes','$eventcode2','$eventperc','1','$date')");
			echo "done";
		}
	}

	if($action == "eventend") {
		$id = $_GET['id'];
		$db->query("UPDATE `EventOffer` SET `offerActive`='0' WHERE `id` = '$id'");
		echo "done";
	}

	if($action == "eventstart") {
		$id = $_GET['id'];
		$db->query("UPDATE `EventOffer` SET `offerActive`='1' WHERE `id` = '$id'");
		echo "done";
	}

	if($action == "eventdelete") {
		$id = $_GET['id'];
		$db->query("DELETE FROM `EventOffer` WHERE `id` = '$id'");
	}

	/* - - - Offer Management - - - */

	if($action == "offercreate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		if($offeruser == "" || $offerperc == "") {
			echo "empty";
		}else{
			$date = date("Y-m-d");
			$db->query("INSERT INTO `Offers`(`id`, `user_id`, `offer_code`, `offer_active`, `offer_percentage`, `offer_used`, `created`) VALUES (NULL,'$offeruser','$offercode2','1','$offerperc','0','$date')");
			echo "done";
		}
	}

	if($action == "offerend") {
		$id = $_GET['id'];
		$db->query("UPDATE `Offers` SET `offer_active`='0' WHERE `id` = '$id'");
		echo "done";
	}

	if($action == "offerstart") {
		$id = $_GET['id'];
		$db->query("UPDATE `Offers` SET `offer_active`='1' WHERE `id` = '$id'");
		echo "done";
	}

	if($action == "offerdelete") {
		$id = $_GET['id'];
		$db->query("DELETE FROM `Offers` WHERE `id` = '$id'");
	}

	/* - - - Profile Actions - - - */

	if($action == "profileupdate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		$date = date("Y-m-d");
		if(trim($prfpass) == ""){
			$db->query("UPDATE `admins` SET `admin_name`='$prfname',`adminEmail`='$prfemail',`admin_number`='$prfnumber',`modified`='$date' WHERE `id` = '$prfid'");
			echo "done";
		}else if(trim($prfpass) != ""){
			$db->query("UPDATE `admins` SET `admin_name`='$prfname',`admin_password`='$prfpass',`adminEmail`='$prfemail',`admin_number`='$prfnumber',`modified`='$date' WHERE `id` = '$prfid'");
			echo "done";
		}else{
			echo "fuck";
		}
	}

	/* - - - Categories - - - */

	if($action == "catcreate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		if($v == ""){
			// echo "لطفا اطلاعات را به صورت کامل وارد کنید";
			echo "empty";
		}else{
			$info = $db->query("SELECT * FROM `categories` WHERE `id`='$catnum'");
			if($info->num_rows == 0) {
				$db->query("INSERT INTO `categories`(`id`, `title`, `dis`) VALUES ('$catnum','$catname','$catdes')");
				// echo "اطلاعات با موفقیت ثبت شد";
				echo "done";
			}else{
				// echo "شماره دسته تکراری است";
				echo "same";
			}
			
		}
	}

	if($action == "catedit") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		if($v == ""){
			echo "لطفا اطلاعات را به صورت کامل وارد کنید";
		}else{
			$db->query("UPDATE `categories` SET `title`='$catname',`dis`='$catdes' WHERE `id`='$catnum2'");
			echo "اطلاعات با موفقیت به روز رسانی شد";
		}
	}

	if($action == "catdelete") {
		if($id = $_GET['id']){
			$db->query("DELETE FROM `categories` WHERE `id` = '$id'");
			echo "done";
		}else{
			echo "bad";
		}
	}

	if($action == "subcatcreate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		if($v == ""){
			// echo "لطفا اطلاعات را به صورت کامل وارد کنید";
			echo "empty";
		}else{
			$info = $db->query("SELECT * FROM `subcat` WHERE `id`='$subcatnumber'");
			if($info->num_rows == 0) {
				$db->query("INSERT INTO `subcat`(`id`, `catId`, `title`, `dis`) VALUES ('$subcatnumber','$subcatcat','$subcatname','$subcatdes')");
				// echo "اطلاعات با موفقیت ثبت شد";
				echo "done";
			}else{
				// echo "شماره زیردسته تکراری است";
				echo "same";
			}
			
		}
	}

	if($action == "subcatdelete") {
		if($id = $_GET['id']){
			$db->query("DELETE FROM `subcat` WHERE `id` = '$id'");
			echo "done";
		}else{
			echo "bad";
		}
	}

	/* - - - Supplier CODES - - - */

	if($action == "supplycreate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		$date = date("Y/m/d");
		if($v == "") {
			echo "empty";
		}else{
			$info = $db->query("SELECT * FROM `supplier` WHERE `code` = '$suppcode'");
			if($info->num_rows == 0) {
				$db->query("INSERT INTO `supplier`(`id`, `code`, `name`, `family`, `groups`, `cellPhone`, `officePhone`, `address`, `created`, `modified`) VALUES (NULL,'$suppcode','$suppname','$suppfamily','$suppgroup','$suppcell','$suppphone','$suppaddr','$date','$date')");
				echo "done";
			}else{
				echo "same";
			}
		}
	}
	
	if($action == "supplyupdate") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		$date = date("Y/m/d");
		if($v == "") {
			echo "empty";
		}else{
			$id = $_GET['id'];
			$db->query("UPDATE `supplier` SET `code`='$suppcode',`name`='$suppname',`family`='$suppfamily',`groups`='$suppgroup',`cellPhone`='$suppcell',`officePhone`='$suppphone',`address`='$suppaddr',`modified`='$date' WHERE `id`='$id'");
			echo "done";
		}
	}

	if($action == "suppdelete") {
		if($id = $_GET['id']){
			$db->query("DELETE FROM `supplier` WHERE `id` = '$id'");
		}
	}

	/* - - - Baskets Codes - - - */

	if($action == "archivebaskets") {
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;					
		}
		$count = count($v);
		for($i = 0; $i < $count; $i++) {
			$arryitem = $v[$i];
			$db->query("UPDATE `baskets` SET `basket_archived`='1' WHERE `id` = '$arryitem'");
		}
		echo "done";
	}

	/* - - - USER CODES - - - */

	if($action == "usercreate"){
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
				$$k = $v;
		}
		$date = date("Y/m/d");
		if($fullname == "" || $username == "" || $password == "" || $repassword == ""){
			echo "empty";
		}else{
			if(trim($password) != trim($repassword)) {
				echo "pass";
			}else{
				$info = $db->query("SELECT * FROM `admins` WHERE `admin_username` = '$username'");
				if($info->num_rows == 0){
					$pass = md5($password);
					$per = [];
					if($proper == 1) {
						array_push($per,"proper");
					}
					if($catper == 1) {
						array_push($per,"catper");
					}
					if($supplierper == 1) {
						array_push($per,"supplierper");
					}
					if($adper == 1) {
						array_push($per,"adper");
					}
					if($cardper == 1) {
						array_push($per,"cardper");
					}
					if($reportper == 1) {
						array_push($per,"reportper");
					}
					if($offper == 1) {
						array_push($per,"offper");
					}
					if($deliveryper == 1) {
						array_push($per,"deliveryper");
					}
					if($adminper == 1) {
						array_push($per,"adminper");
					}
					$pergo = implode(",",$per);
					$db->query("INSERT INTO `admins`(`id`, `admin_name`, `admin_username`, `admin_password`, `admin_active`, `adminEmail`, `admin_number`, `adminPer`, `created`, `modified`) VALUES (NULL,'$fullname','$username','$pass','1','0','0','$pergo','$date','$date')");
					echo "done";
				}else{
					echo "username";
				}
			}
		}
	}

	if($action == "userdelete"){
		if($id = $_GET['id']){
			$db->query("DELETE FROM `admins` WHERE `id` = '$id'");
			echo "done";
		}else{
			echo "fuck";
		}
	}

	// if($action == "userupdate"){
	// 	foreach($_POST as $k=>$v) {
	// 		if(!is_array($v)) {
	// 			$$k = $v;
	// 		}
	// 	}
	// 	$userpers = $v[0];
	// 	$userid = $v[1];
	// 	$fullname = $v[2];
	// 	$username = $v[3];
	// 	$usermail = $v[4];
	// 	$userphone = $v[5];
	// 	$password = $v[6];
	// 	$repassword = $v[7];
		
	// 	if($password == ""){
	// 		if($fullname == ""){
	// 			echo "empty";
	// 		}else{
	// 			$date = date("Y/m/d");
	// 			$info = $db->query("UPDATE `admins` SET `admin_name`='$fullname',`adminEmail`='$usermail',`admin_number`='$userphone',`adminPer`='$userpers',`modified`='$date' WHERE `id` = '$userid'");
	// 			echo "done";
	// 		}
	// 	}elseif($password != ""){
	// 		if($password != $repassword) {
	// 			echo "notsamepass";
	// 		}else{
	// 			if($fullname == ""){
	// 				echo "empy";
	// 			}else{
	// 				$pass = md5($password);
	// 				$date = date("Y/m/d");
	// 				$info = $db->query("UPDATE `admins` SET `admin_name`='$fullname',`admin_password`='$pass',`adminEmail`='$usermail',`admin_number`='$userphone',`adminPer`='$userpers',`modified`='$date' WHERE `id` = '$userid'");
	// 				echo "done";
	// 			}
	// 		}
	// 	}
	// }
?>