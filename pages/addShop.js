import React, { Component } from "react"
import styled from "styled-components"
import MyLayout from "../components/main/MyLayout"
import Loading from "../components/main/Loading"
import Router from "next/router"
import { postData } from "../utils/main"
import { Formik } from "formik"

export default class AddShop extends Component {
  state = {
    logged: 0,
    msgstatus: "none",
    msgcolor: "#fff",
    msgtext: " "
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  componentDidMount() {
    this.loginCheck()
  }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Loading />
      ) : (
        <MyLayout>
          <AddShopWrapper>
            <AddShopTitle>افزودن فروشگاه جدید</AddShopTitle>
            <Formik
              initialValues={{ name: "", manager: "" }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  postData(
                    "https://ser01.mrezak.ir/update.php?action=addshop",
                    values
                  ).then(data => {
                    if (data[0] == "done") {
                      this.setState({
                        msgstatus: "block",
                        msgcolor: "#36B37E",
                        msgtext: "فروشگاه مورد نظر با موفقیت ثبت شد"
                      })
                    } else if (data[0] == "empty") {
                      this.setState({
                        msgstatus: "block",
                        msgcolor: "#FF5630",
                        msgtext:
                          "لطفا فیلد های مورد نظر را به صورت کامل پر کنید"
                      })
                    }
                  })
                  setSubmitting(false)
                }, 400)
              }}>
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting
                /* and other goodies */
              }) => (
                <AddShopForm onSubmit={handleSubmit}>
                  <span>نام فروشگاه</span>
                  <AddShopItemContainer>
                    <input
                      type='text'
                      name='name'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.name}
                    />
                    {/* {errors.username && touched.username && errors.username} */}
                  </AddShopItemContainer>
                  <span>مدیریت فروشگاه</span>
                  <AddShopItemContainer>
                    <input
                      type='text'
                      name='manager'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.manager}
                    />
                    {/* {errors.password && touched.password && errors.password} */}
                  </AddShopItemContainer>
                  <AddShopBTN type='submit' disabled={isSubmitting}>
                    ثبت
                  </AddShopBTN>
                </AddShopForm>
              )}
            </Formik>
            <LoginMsgBox
              msgstatus={this.state.msgstatus}
              msgcolor={this.state.msgcolor}>
              <p>{this.state.msgtext}</p>
            </LoginMsgBox>
          </AddShopWrapper>
        </MyLayout>
      )}
    </>
  )
}

const AddShopWrapper = styled.div`
  width: 100%;
  height: 100%;
  direction: rtl;
  background: url("../static/images/addShop.jpg");
  background-size: cover !important;
  background-position: 0px -250px !important;
`
const AddShopTitle = styled.div`
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding-right: 10px;
  direction: rtl;
  background: #ffffff;
  border-top: 2px solid #333;
  border-bottom: 2px solid #43ab92;
`
const AddShopForm = styled.form`
  width: 320px;
  height: 320px;
  border-radius: 10px;
  background: #ff991f;
  box-shadow: 0px 0px 5px #333;
  margin-top: 80px;
  margin-right: 100px;
  overflow: hidden;
  background: url("../static/images/boxBg_320.png") no-repeat;
  span {
    display: block;
    width: 100%;
    height: 40px;
    line-height: 40px;
    padding-right: 10px;
  }
  span:nth-child(1) {
    margin-top: 65px;
  }
`
const AddShopItemContainer = styled.div`
  width: 300px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  border-radius: 5px;
  input {
    width: 100%;
    height: 100%;
    padding: 0;
    box-sizing: border-box;
    padding-right: 5px;
    direction: rtl;
    margin: 0;
    float: right;
    background: none;
    border: medium none;
    outline: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopBTN = styled.button`
  width: 100px;
  height: 36px;
  display: block;
  background: #43ab92;
  border-radius: 5px;
  border: medium none;
  text-align: center;
  line-height: 36px;
  color: #fff;
  font-family: BYekan;
  float: right;
  margin-top: 30px;
  margin-right: 10px;
  cursor: pointer;
`
const LoginMsgBox = styled.div`
  width: 350px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 40px auto;
  text-align: center;
  line-height: 40px;
  color: #fff;
  border-radius: 5px;
`
