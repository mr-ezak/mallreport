import react, { Component } from "react"
import styled from "styled-components"
import { Formik } from "formik"
import { UsernameIcon, PasswordIcon } from "../components/main/Icons"
import { postData } from "../utils/main"
import Router from "next/router"

export default class Login extends Component {
  state = {
    msgstatus: "none",
    msgcolor: "#fff",
    msgtext: " "
  }

  render = () => (
    <div>
      <LoginBox>
        {/* <LoginLogo>
          <img src='/static/images/logo.png' />
        </LoginLogo> */}
        <Formik
          initialValues={{ username: "", password: "" }}
          onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
              postData(
                "https://ser01.mrezak.ir/update.php?action=login",
                values
              ).then(data => {
                if (data[0] == "logged") {
                  window.localStorage.setItem("mallreporttoken", data[1])
                  Router.push("/")
                } else {
                  console.log(data)
                }
              })
              setSubmitting(false)
            }, 400)
          }}>
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting
            /* and other goodies */
          }) => (
            <LoginForm onSubmit={handleSubmit}>
              <LogItemContainer>
                <UsernameIcon />
                <input
                  type='username'
                  name='username'
                  placeholder='نام کاربری'
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.username}
                />
                {/* {errors.username && touched.username && errors.username} */}
              </LogItemContainer>
              <LogItemContainer>
                <PasswordIcon />
                <input
                  type='password'
                  name='password'
                  placeholder='رمز عبور'
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {/* {errors.password && touched.password && errors.password} */}
              </LogItemContainer>
              <LoginBTN type='submit' disabled={isSubmitting}>
                ورود
              </LoginBTN>
            </LoginForm>
          )}
        </Formik>
        <LoginMsgBox
          msgstatus={this.state.msgstatus}
          msgcolor={this.state.msgcolor}>
          <p>{this.state.msgtext}</p>
        </LoginMsgBox>
      </LoginBox>
    </div>
  )
}

const LoginBox = styled.div`
  width: 350px;
  height: 230px;
  margin: 150px auto;
  overflow: hidden;
  background: #ff991f;
  border-radius: 15px;
`
const LoginLogo = styled.div`
  width: 100px;
  height: 100px;
  margin: 20px auto;
  border-radius: 50px;
  img {
    display: block;
    width: 100px;
    height: 100px;
  }
`
const LoginForm = styled.form`
  width: 100%;
  height: auto;
  overflow: hidden;
  span {
    display: block;
    width: 100%;
    height: 30px;
  }
`
const LogItemContainer = styled.div`
  width: 90%;
  height: 40px;
  border-bottom: 1px solid #333;
  margin: 20px auto;
  svg {
    width: 20px;
    height: 20px;
    float: right;
    margin-left: 5px;
    margin-top: 10px;
  }
  input {
    width: 87%;
    padding-right: 5px;
    direction: rtl;
    margin-top: 5px;
    height: 30px;
    float: right;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
  }
  input::placeholder {
    color: #333;
    opacity: 1;
    font-size: 12px;
    font-family: BYekan;
  }
`
const LoginBTN = styled.button`
  width: 150px;
  height: 40px;
  display: block;
  background: #67b0af;
  border-radius: 10px;
  border: medium none;
  text-align: center;
  line-height: 40px;
  color: #fff;
  font-family: BYekan;
  margin: 40px auto;
  cursor: pointer;
`
const LoginMsgBox = styled.div`
  width: 200px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 40px auto;
  text-align: center;
  line-height: 40px;
  color: #fff;
  border-radius: 5px;
`
