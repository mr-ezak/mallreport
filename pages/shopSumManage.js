import styled from "styled-components"
import MyLayout from "../components/main/MyLayout"
import { Component } from "react"
import { TrashIcon, EditIcon } from "../components/main/Icons"
import { postData } from "../utils/main"
import Loading from "../components/main/Loading"
import { ShopEdit, ShopEditBlank } from "../components/shopmanage/ShopEdit"

export default class ShopSumManage extends Component {
  state = {
    logged: 0,
    ShopEditTop: -300,
    ShopEditDisplay: "none",
    shopEditId: "",
    shopEditName: "",
    shopEditManager: "",
    shops: []
  }

  shopSumReceiver = () => {
    postData("https://ser01.mrezak.ir/update.php?action=shopsumreq").then(
      data => {
        this.setState({
          shops: data["info"]
        })
      }
    )
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
            this.shopSumReceiver()
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  handleSumRemove = id => {
    let confirm = window.confirm("آیا از حذف این رکورد اطمینان دارید؟")
    if (confirm == true) {
      postData(
        "https://ser01.mrezak.ir/update.php?action=sumremove&id=" + id
      ).then(data => {
        if (data == "done") {
          let row = document.getElementById(id)
          row.setAttribute("class", "remove")
        }
      })
    }
  }

  handleShopEditClick = id => {
    postData(
      "https://ser01.mrezak.ir/update.php?action=shopeditinfo&id=" + id
    ).then(data => {
      this.setState({
        shopEditId: data.id,
        shopEditName: data.name,
        shopEditManager: data.manager,
        ShopEditTop: 200,
        ShopEditDisplay: "block"
      })
    })
  }

  handleShopEditCloser = () => {
    this.setState({
      ShopEditTop: -230,
      ShopEditDisplay: "none"
    })
  }

  componentDidMount() {
    this.loginCheck()
  }

  render = () => (
    <>
      <MyLayout>
        {this.state.shops.length == 0 ? (
          <Loading />
        ) : (
          <ShopsManageWrapper>
            <ShopEdit
              top={this.state.ShopEditTop}
              id={this.state.shopEditId}
              name={this.state.shopEditName}
              manager={this.state.shopEditManager}
            />
            <ShopEditBlank
              display={this.state.ShopEditDisplay}
              onClick={this.handleShopEditCloser}
            />
            <ShopsManageTitle>مدیریت مجموع فروش</ShopsManageTitle>
            <ShopsManageContainer>
              <table>
                <tr>
                  <th>نام فروشگاه</th>
                  <th>مبلغ</th>
                  <th>نوع</th>
                  <th>تاریخ تراکنش</th>
                  <th>اعمال</th>
                </tr>
                {this.state.shops.map((data, index) => (
                  <tr key={index} id={data.id}>
                    <td>{data.name}</td>
                    <td>{data.price}</td>
                    <td>
                      {data.type == "p" ? "مجموع فروش" : "برداشت از موجودی"}
                    </td>
                    <td>{data.created}</td>
                    <td>
                      <TrashIcon
                        onClick={() => this.handleSumRemove(data.id)}
                      />
                    </td>
                    {/* <ServicesSingleBTN
                      onClick={() => this.serviceSingleGo(data.id)}>
                      درخواست
                    </ServicesSingleBTN> */}
                  </tr>
                ))}
              </table>
            </ShopsManageContainer>
          </ShopsManageWrapper>
        )}
      </MyLayout>
    </>
  )
}

const ShopsManageWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: #fff;
`
const ShopsManageTitle = styled.div`
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding-right: 10px;
  direction: rtl;
  background: #fff;
  border-top: 2px solid #333;
  border-bottom: 2px solid #ff991f;
  float: right;
`
const ShopsManageContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin-top: 10px;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;
  table {
    width: 97%;
    height: auto;
    padding: 5px;
    margin: 20px auto;
    border-spacing: 5px;
    box-sizing: border-box;
    box-shadow: 0px 0px 5px #333;
    border-radius: 5px;
    tr[class="remove"] {
      display: none;
    }
    tr {
      width: 99%;
      height: 50px;
      padding-right: 10px;
      line-height: 50px;
      display: block;
      margin-top: 5px;
      th {
        width: 250px;
        text-align: right;
      }
      th:last-child {
        width: 200px;
        text-align: left;
      }
      td {
        width: 250px;
        svg {
          cursor: pointer;
          margin-right: 10px;
        }
      }
      td:last-child {
        width: 230px;
        text-align: left;
      }
    }
    tr:nth-child(2n + 1) {
      background: rgba(67, 171, 146, 0.5);
    }
  }
`
