import styled from "styled-components"
import MyLayout from "../components/main/MyLayout"
import { Component } from "react"
import { TrashIcon, EditIcon } from "../components/main/Icons"
import { postData } from "../utils/main"
import Loading from "../components/main/Loading"
import { UserEdit, UserEditBlank } from "../components/usermanage/UserEdit"

export default class UsersManage extends Component {
  state = {
    logged: 0,
    UserEditTop: -300,
    UserEditDisplay: "none",
    UserEditId: "",
    UserEditFullName: "",
    UserEditUserName: "",
    UserEditPassword: "",
    users: []
  }

  usersReceiver = () => {
    postData("https://ser01.mrezak.ir/update.php?action=userslistreq").then(
      data => {
        this.setState({
          users: data["info"]
        })
      }
    )
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
            this.usersReceiver()
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  handleUserRemove = id => {
    let confirm = window.confirm("آیا از حذف این فروشگاه اطمینان دارید؟")
    if (confirm == true) {
      postData(
        "https://ser01.mrezak.ir/update.php?action=userremove&id=" + id
      ).then(data => {
        if (data == "done") {
          let row = document.getElementById(id)
          row.setAttribute("class", "remove")
        }
      })
    }
  }

  handleUserEditClick = id => {
    postData(
      "https://ser01.mrezak.ir/update.php?action=usereditinfo&id=" + id
    ).then(data => {
      this.setState({
        UserEditId: data.id,
        UserEditFullName: data.fullName,
        UserEditUserName: data.username,
        UserEditTop: 200,
        UserEditDisplay: "block"
      })
    })
  }

  handleUserEditCloser = () => {
    this.setState({
      UserEditTop: -300,
      UserEditDisplay: "none"
    })
  }

  componentDidMount() {
    this.loginCheck()
  }

  render = () => (
    <>
      <MyLayout>
        {this.state.users.length == 0 ? (
          <Loading />
        ) : (
          <ShopsManageWrapper>
            <UserEdit
              top={this.state.UserEditTop}
              id={this.state.UserEditId}
              fullname={this.state.UserEditFullName}
              username={this.state.UserEditUserName}
            />
            <UserEditBlank
              display={this.state.UserEditDisplay}
              onClick={this.handleUserEditCloser}
            />
            <ShopsManageTitle>مدیریت کاربران</ShopsManageTitle>
            <ShopsManageContainer>
              <table>
                <tr>
                  <th>نام کاربر</th>
                  <th>نام کاربری</th>
                  <th>تاریخ ایجاد</th>
                  <th>اعمال</th>
                </tr>
                {this.state.users.map((data, index) => (
                  <tr key={index} id={data.id}>
                    <td>{data.fullName}</td>
                    <td>{data.username}</td>
                    <td>{data.created}</td>
                    <td>
                      <TrashIcon
                        onClick={() => this.handleUserRemove(data.id)}
                      />
                      <EditIcon
                        onClick={() => this.handleUserEditClick(data.id)}
                      />
                    </td>
                    {/* <ServicesSingleBTN
                      onClick={() => this.serviceSingleGo(data.id)}>
                      درخواست
                    </ServicesSingleBTN> */}
                  </tr>
                ))}
              </table>
            </ShopsManageContainer>
          </ShopsManageWrapper>
        )}
      </MyLayout>
    </>
  )
}

const ShopsManageWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  background: #eceff4;
`
const ShopsManageTitle = styled.div`
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding-right: 10px;
  direction: rtl;
  background: #fff;
  border-top: 2px solid #333;
  border-bottom: 2px solid #ff991f;
  float: right;
`
const ShopsManageContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin-top: 10px;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;
  table {
    width: 97%;
    height: auto;
    padding: 5px;
    margin: 20px auto;
    border-spacing: 5px;
    box-shadow: 0px 0px 5px #333;
    border-radius: 5px;
    tr[class="remove"] {
      display: none;
    }
    tr {
      width: 99%;
      height: 50px;
      padding-right: 10px;
      line-height: 50px;
      display: block;
      margin-top: 5px;
      th {
        width: 350px;
        text-align: right;
      }
      th:last-child {
        width: 170px;
        text-align: left;
      }
      td {
        width: 350px;
        svg {
          cursor: pointer;
          margin-right: 10px;
        }
      }
      td:last-child {
        width: 200px;
        text-align: left;
      }
    }
    tr:nth-child(2n + 1) {
      background: rgba(67, 171, 146, 0.5);
    }
    // tr:nth-child(2n) {
    //   background: rgba(67, 171, 146, 0.6);
    // }
  }
`
