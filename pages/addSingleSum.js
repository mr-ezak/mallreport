import React, { Component } from "react"
import styled from "styled-components"
import MyLayout from "../components/main/MyLayout"
import Loading from "../components/main/Loading"
import Router from "next/router"
import { postData } from "../utils/main"
import Select from "react-select"
import moment from "moment-jalaali"
import DatePicker from "react-datepicker2"

// const options = [
//   { value: "chocolate", label: "Chocolate" },
//   { value: "strawberry", label: "Strawberry" },
//   { value: "vanilla", label: "Vanilla" }
// ]

// const customStyles = {
//   control: () => ({
//     height: 40
//   })
// }

export default class AddSingleSum extends Component {
  state = {
    logged: 0,
    msgstatus: "none",
    msgcolor: "#fff",
    msgtext: " ",
    shops: [],
    options: [],
    selectedOption: null,
    sumPrice: "",
    startDate: "",
    endDate: ""
  }

  shopsReceiver = () => {
    postData("https://ser01.mrezak.ir/update.php?action=shopslistreq").then(
      data => {
        this.setState({
          shops: data["info"]
        })
        let count = data["info"].length
        let i
        let options = []
        let element = {}
        if (count > 0) {
          for (i = 0; i < count; i++) {
            let single = data["info"]
            let cur = single[i]
            element.value = cur.id
            element.label = cur.name
            options.push({ value: element.value, label: element.label })
          }
          this.setState({
            options: options
          })
        }
      }
    )
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
            this.shopsReceiver()
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  componentDidMount() {
    this.loginCheck()
  }

  handleAddSingleSum = () => {
    if (this.state.selectedOption == null) {
      this.setState({
        msgstatus: "block",
        msgcolor: "#FF5630",
        msgtext: "لطفا ابتدا مغازه ای انتخاب کنید"
      })
    } else {
      const userToken = window.localStorage.getItem("mallreporttoken")
      let sumData = {
        userToken: userToken,
        shopId: this.state.selectedOption.value,
        sumPrice: this.state.sumPrice,
        fromDate: this.state.startDate,
        toDate: this.state.endDate
      }
      postData(
        "https://ser01.mrezak.ir/update.php?action=addshopsum",
        sumData
      ).then(data => {
        if (data[0] == "done") {
          this.setState({
            msgstatus: "block",
            msgcolor: "#36B37E",
            msgtext: "مجموع فروش مورد نظر با موفقیت ثبت شد"
          })
        } else if (data[0] == "emp") {
          this.setState({
            msgstatus: "block",
            msgcolor: "#FF5630",
            msgtext: "لطفا فیلد های مورد نظر را به صورت کامل پر کنید"
          })
        }
      })
    }
  }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Loading />
      ) : (
        <MyLayout>
          <AddShopWrapper>
            <AddShopTitle>ثبت مجموع فروش</AddShopTitle>
            <AddShopForm>
              <span class='first'>نام فروشگاه</span>
              <AddShopItemContainer>
                <Select
                  // styles={customStyles}
                  options={this.state.options}
                  onChange={this.handleChange}
                  type='name'
                  name='name'>
                  >
                </Select>
              </AddShopItemContainer>
              <span>مبلغ مجموع فروش</span>
              <AddShopItemContainer>
                <input
                  type='number'
                  name='manager'
                  onChange={() =>
                    this.setState({ sumPrice: event.target.value })
                  }
                />
              </AddShopItemContainer>
              <span>از تاریخ</span>
              <AddShopFromDateContainer>
                <DatePicker
                  selectedDay={this.state.startDate}
                  timePicker={false}
                  isGregorian={false}
                  onChange={value =>
                    this.setState({
                      startDate: value.locale("fa").format("jYYYY/jM/jD")
                    })
                  }
                />
              </AddShopFromDateContainer>
              <span>تا تاریخ</span>
              <AddShopToDateContainer>
                <DatePicker
                  selectedDay={this.state.endDate}
                  timePicker={false}
                  isGregorian={false}
                  onChange={value =>
                    this.setState({
                      endDate: value.locale("fa").format("jYYYY/jM/jD")
                    })
                  }
                />
              </AddShopToDateContainer>
              <AddShopBTN onClick={this.handleAddSingleSum}>ثبت</AddShopBTN>
            </AddShopForm>
            <LoginMsgBox
              msgstatus={this.state.msgstatus}
              msgcolor={this.state.msgcolor}>
              <p>{this.state.msgtext}</p>
            </LoginMsgBox>
          </AddShopWrapper>
        </MyLayout>
      )}
    </>
  )
}

const AddShopWrapper = styled.div`
  width: 100%;
  height: 100%;
  direction: rtl;
  background: url("../static/images/AddSingleSum.jpg");
  background-size: cover !important;
  background-position: 0px -170px !important;
  div:nth-child(4) {
    margin-bottom: 0px;
  }
`
const AddShopTitle = styled.div`
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding-right: 10px;
  direction: rtl;
  background: #ffffff;
  border-top: 2px solid #333;
  border-bottom: 2px solid #333;
`
const AddShopForm = styled.form`
  width: 320px;
  height: 470px;
  background: url("../static/images/boxBg_470.png") no-repeat;
  border-radius: 10px;
  margin-top: 20px;
  margin-right: 100px;
  overflow: hidden;
  box-shadow: 0px 0px 5px #333;
  span {
    display: block;
    width: 100%;
    height: 40px;
    line-height: 40px;
    padding-right: 10px;
  }
  span[class="first"] {
    margin-top: 60px;
  }
`
const AddShopItemContainer = styled.div`
  width: 300px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  margin-bottom: 20px;
  border-radius: 5px;
  select {
    width: 100%;
    height: 30px;
    padding-right: 5px;
    direction: rtl;
    margin-top: 5px;
    float: right;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
  }
  input[type="number"] {
    appearance: textfield;
  }
  input {
    width: 100%;
    height: 40px;
    padding: 0;
    padding-right: 5px;
    direction: rtl;
    float: right;
    box-sizing: border-box;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopFromDateContainer = styled.div`
  width: 300px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  margin-bottom: 0px;
  border-radius: 5px;
  input {
    width: 100%;
    height: 40px;
    padding: 0;
    padding-right: 5px;
    direction: rtl;
    float: right;
    box-sizing: border-box;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopToDateContainer = styled.div`
  width: 300px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  border-radius: 5px;
  input {
    width: 100%;
    height: 40px;
    padding: 0;
    padding-right: 5px;
    direction: rtl;
    float: right;
    box-sizing: border-box;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopBTN = styled.div`
  width: 100px;
  height: 36px;
  display: block;
  background: #43ab92;
  border-radius: 5px;
  border: medium none;
  text-align: center;
  line-height: 36px;
  color: #fff;
  font-family: BYekan;
  float: right;
  margin-top: 20px;
  margin-right: 10px;
  cursor: pointer;
`
const LoginMsgBox = styled.div`
  width: 350px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: -40px auto;
  text-align: center;
  line-height: 40px;
  color: #fff;
  border-radius: 5px;
`
