import React, { Component } from "react"
import MyLayout from "../components/main/MyLayout"
import Loading from "../components/main/Loading"
import Router from "next/router"
import { postData } from "../utils/main"
import { Dashboard } from "../components/main/Dashboard"

export default class Index extends Component {
  state = {
    logged: 0
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  componentDidMount() {
    this.loginCheck()
  }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Loading />
      ) : (
        <MyLayout>
          <Dashboard />
        </MyLayout>
      )}
    </>
  )
}
