import React, { Component } from "react"
import styled from "styled-components"
import MyLayout from "../components/main/MyLayout"
import Loading from "../components/main/Loading"
import Router from "next/router"
import { postData } from "../utils/main"
import Select from "react-select"
import moment from "moment-jalaali"
import DatePicker from "react-datepicker2"

export default class SumReport extends Component {
  state = {
    logged: 0,
    msgstatus: "none",
    msgcolor: "#fff",
    msgtext: " ",
    shops: [],
    options: [],
    selectedOption: null,
    startDate: "",
    endDate: "",
    reports: "",
    allsum: "",
    taken: ""
  }

  shopsReceiver = () => {
    postData("https://ser01.mrezak.ir/update.php?action=shopslistreq").then(
      data => {
        this.setState({
          shops: data["info"]
        })
        let count = data["info"].length
        let i
        let options = []
        let element = {}
        if (count > 0) {
          for (i = 0; i < count; i++) {
            let single = data["info"]
            let cur = single[i]
            element.value = cur.id
            element.label = cur.name
            options.push({ value: element.value, label: element.label })
          }
          this.setState({
            options: options
          })
        }
      }
    )
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
            this.shopsReceiver()
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  componentDidMount() {
    this.loginCheck()
  }

  handleAddSingleSum = () => {
    // Set selected Option for sending
    let sumData
    if (this.state.selectedOption == null) {
      sumData = {
        shopId: "0",
        fromDate: this.state.startDate,
        toDate: this.state.endDate
      }
    } else {
      sumData = {
        shopId: this.state.selectedOption.value,
        fromDate: this.state.startDate,
        toDate: this.state.endDate
      }
    }
    //Send the req for the server
    postData(
      "https://ser01.mrezak.ir/update.php?action=sumreport",
      sumData
    ).then(data => {
      if (data[1] == "") {
        this.setState({
          reports: ""
        })
      } else {
        this.setState({
          reports: data[0],
          allsum: data[1],
          taken: data[2]
        })
      }
    })
  }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Loading />
      ) : (
        <MyLayout>
          <AddShopWrapper>
            <AddShopTitle>گزارش گیری</AddShopTitle>
            <AddShopForm>
              <span class='first'>نام فروشگاه</span>
              <AddShopItemContainer>
                <Select
                  // styles={customStyles}
                  options={this.state.options}
                  onChange={this.handleChange}
                  type='name'
                  name='name'>
                  >
                </Select>
              </AddShopItemContainer>
              <span>از تاریخ</span>
              <AddShopFromDateContainer>
                <DatePicker
                  selectedDay={this.state.startDate}
                  timePicker={false}
                  isGregorian={false}
                  onChange={value =>
                    this.setState({
                      startDate: value.locale("fa").format("jYYYY/jM/jD")
                    })
                  }
                />
              </AddShopFromDateContainer>
              <span>تا تاریخ</span>
              <AddShopToDateContainer>
                <DatePicker
                  selectedDay={this.state.endDate}
                  timePicker={false}
                  isGregorian={false}
                  onChange={value =>
                    this.setState({
                      endDate: value.locale("fa").format("jYYYY/jM/jD")
                    })
                  }
                />
              </AddShopToDateContainer>
              <AddShopBTN onClick={this.handleAddSingleSum}>شروع</AddShopBTN>
            </AddShopForm>
            <LoginMsgBox
              msgstatus={this.state.msgstatus}
              msgcolor={this.state.msgcolor}>
              <p>{this.state.msgtext}</p>
            </LoginMsgBox>
          </AddShopWrapper>
          {this.state.reports == "" ? (
            <EmptyTable>هنوز گزارشی درخواست نشده است</EmptyTable>
          ) : (
            <ShopsManageContainer>
              <TableTitle>
                <span>مجموع کل فروش : </span>
                <div> {this.state.allsum} </div>
                <span>گرفته شده در تاریخ :‌ </span>
                <div> {this.state.taken} </div>
              </TableTitle>
              <table>
                <tr>
                  <th>نام فروشگاه</th>
                  <th>مدیریت</th>
                  <th>مجموع فروش</th>
                </tr>
                {this.state.reports.map((data, index) => (
                  <tr key={index} id={data.id}>
                    <td>{data.name}</td>
                    <td>{data.manager}</td>
                    <td>{data.result}</td>
                  </tr>
                ))}
              </table>
            </ShopsManageContainer>
          )}
        </MyLayout>
      )}
    </>
  )
}

const AddShopWrapper = styled.div`
  width: 100%;
  height: 200px;
  direction: rtl;
  background: #fff;
  background-size: cover !important;
  background-position: 0px -170px !important;
  div:nth-child(4) {import SumReport from './sumReport';

    margin-bottom: 0px;
  }
`
const ShopsManageContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin: 0px auto;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;
  table {
    width: 97%;
    height: auto;
    padding: 5px;
    margin: 20px auto;
    border-spacing: 5px;
    box-shadow: 0px 0px 5px #333;
    border-radius: 5px;
    tr[class="remove"] {
      display: none;
    }
    tr {
      width: 99%;
      height: 50px;
      padding-right: 10px;
      line-height: 50px;
      display: block;
      margin-top: 5px;
      th {
        width: 350px;
        text-align: right;
      }
      th:last-child {
        width: 170px;
        text-align: left;
      }
      td {
        width: 350px;
        svg {
          cursor: pointer;
          margin-right: 10px;
        }
      }
      td:last-child {
        width: 200px;
        text-align: left;
      }
    }
    tr:nth-child(2n + 1) {
      background: rgba(67, 171, 146, 0.5);
    }
    // tr:nth-child(2n + 1) {
    //   background: rgba(255, 153, 31, 0.6);
    // }
    // tr:nth-child(2n) {
    //   background: rgba(67, 171, 146, 0.6);
    // }
  }
`
const TableTitle = styled.div`
  width: 97%;
  height: 100px;
  margin: 0px auto;
  span {
    display: block;
    height: 36px;
    text-align: right;
    padding-right: 10px;
    line-height: 36px;
    float: right;
  }
  div {
    display: block;
    width: 100%;
    height: 36px;
    text-align: right;
    padding-right: 10px;
    line-height: 36px;
  }
`
const EmptyTable = styled.div`
  width: 100%;
  height: 40px;
  text-align: center;
  line-height: 40px;
`
const AddShopTitle = styled.div`
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding-right: 10px;
  direction: rtl;
  background: #ffffff;
  border-top: 2px solid #333;
  border-bottom: 2px solid #333;
`
const AddShopForm = styled.form`
  width: 95%;
  height: 90px;
  background: #fff;
  border-radius: 10px;
  margin: 20px auto;
  box-shadow: 0px 0px 5px #333;
  span {
    display: block;
    float: right;
    width: 100px;
    height: 40px;
    text-align: center;
    margin-top: 20px;
    line-height: 40px;
    padding-right: 10px;
  }
`
const AddShopItemContainer = styled.div`
  width: 300px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  margin-top: 10px;
  border-radius: 5px;
  float: right;
  select {
    width: 100%;
    height: 30px;
    padding-right: 5px;
    direction: rtl;
    margin-top: 5px;
    float: right;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
  }
  input[type="number"] {
    appearance: textfield;
  }
  input {
    width: 100%;
    height: 40px;
    padding: 0;
    padding-right: 5px;
    direction: rtl;
    float: right;
    box-sizing: border-box;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopFromDateContainer = styled.div`
  width: 150px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  margin-top: 20px;
  border-radius: 5px;
  float: right;
  input {
    width: 100%;
    height: 40px;
    padding: 0;
    padding-right: 5px;
    direction: rtl;
    float: right;
    box-sizing: border-box;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopToDateContainer = styled.div`
  width: 150px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  margin-top: 20px;
  border-radius: 5px;
  float: right;
  input {
    width: 100%;
    height: 40px;
    padding: 0;
    padding-right: 5px;
    direction: rtl;
    float: right;
    box-sizing: border-box;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddShopBTN = styled.div`
  width: 100px;
  height: 36px;
  display: block;
  background: #43ab92;
  border-radius: 5px;
  border: medium none;
  text-align: center;
  line-height: 36px;
  color: #fff;
  font-family: BYekan;
  float: right;
  margin-top: 20px;
  margin-right: 100px;
  cursor: pointer;
`
const LoginMsgBox = styled.div`
  width: 350px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: -20px auto;
  text-align: center;
  line-height: 40px;
  color: #fff;
  border-radius: 5px;
`
