import React, { Component } from "react"
import styled from "styled-components"
import MyLayout from "../components/main/MyLayout"
import Loading from "../components/main/Loading"
import Router from "next/router"
import { postData } from "../utils/main"
import { Formik } from "formik"

export default class AddUser extends Component {
  state = {
    logged: 0,
    msgstatus: "none",
    msgcolor: "#fff",
    msgtext: " "
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("mallreporttoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("mallreporttoken")
        postData(
          "https://ser01.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  componentDidMount() {
    this.loginCheck()
  }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Loading />
      ) : (
        <MyLayout>
          <AddUserWrapper>
            <AddUserTitle>افزودن کاربر جدید</AddUserTitle>
            <Formik
              initialValues={{
                fullname: "",
                username: "",
                password: "",
                permission: ""
              }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  postData(
                    "https://ser01.mrezak.ir/update.php?action=adduser",
                    values
                  ).then(data => {
                    if (data[0] == "done") {
                      this.setState({
                        msgstatus: "block",
                        msgcolor: "#36B37E",
                        msgtext: "کاربر مورد نظر با موفقیت ثبت شد"
                      })
                    } else if (data[0] == "emp") {
                      this.setState({
                        msgstatus: "block",
                        msgcolor: "#FF5630",
                        msgtext:
                          "لطفا فیلد های مورد نظر را به صورت کامل پر کنید"
                      })
                    } else if (data[0] == "exist") {
                      this.setState({
                        msgstatus: "block",
                        msgcolor: "#FF5630",
                        msgtext: "ورود اطلاعت تکراری"
                      })
                    }
                  })
                  setSubmitting(false)
                }, 400)
              }}>
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting
                /* and other goodies */
              }) => (
                <AddUserForm onSubmit={handleSubmit}>
                  <span>نام کامل</span>
                  <AddUserItemContainer>
                    <input
                      type='text'
                      name='fullname'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.fullname}
                    />
                    {/* {errors.username && touched.username && errors.username} */}
                  </AddUserItemContainer>
                  <span>نام کاربری</span>
                  <AddUserItemContainer>
                    <input
                      type='text'
                      name='username'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.username}
                    />
                    {/* {errors.username && touched.username && errors.username} */}
                  </AddUserItemContainer>
                  <span>رمز عبور</span>
                  <AddUserItemContainer>
                    <input
                      type='text'
                      name='password'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.password}
                    />
                    {/* {errors.password && touched.password && errors.password} */}
                  </AddUserItemContainer>
                  <span>سطح دسترسی</span>
                  <AddUserSelectContainer>
                    <select
                      name='permission'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.permission}>
                      <option></option>
                      <option value='1'>مدیر سیستم</option>
                      <option value='2'>کاربر سطح اول</option>
                      <option value='3'>کاربر سطح دوم</option>
                    </select>
                  </AddUserSelectContainer>
                  <AddUserBTN type='submit' disabled={isSubmitting}>
                    ثبت
                  </AddUserBTN>
                </AddUserForm>
              )}
            </Formik>
            <LoginMsgBox
              msgstatus={this.state.msgstatus}
              msgcolor={this.state.msgcolor}>
              <p>{this.state.msgtext}</p>
            </LoginMsgBox>
          </AddUserWrapper>
        </MyLayout>
      )}
    </>
  )
}

const AddUserWrapper = styled.div`
  width: 100%;
  height: 100%;
  direction: rtl;
  background: url("../static/images/AddUser.jpg");
  background-size: cover !important;
  background-position: 0px -170px !important;
`
const AddUserTitle = styled.div`
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding-right: 10px;
  direction: rtl;
  background: #ffffff;
  border-top: 2px solid #333;
  border-bottom: 2px solid #43ab92;
`
const AddUserForm = styled.form`
  width: 320px;
  height: 420px;
  border-radius: 10px;
  background: url("../static/images/boxBg_380.png") no-repeat;
  box-shadow: 0px 0px 5px #333;
  margin-top: 20px;
  margin-right: 100px;
  overflow: hidden;
  span {
    display: block;
    width: 100%;
    height: 34px;
    line-height: 34px;
    padding-right: 10px;
  }
  span:nth-child(1) {
    margin-top: 62px;
  }
`
const AddUserItemContainer = styled.div`
  width: 300px;
  height: 40px;
  background: #fff;
  margin-right: 10px;
  border-radius: 5px;
  input {
    width: 100%;
    height: 100%;
    padding: 0;
    box-sizing: border-box;
    padding-right: 5px;
    direction: rtl;
    margin: 0;
    float: right;
    background: none;
    border: medium none;
    outline: medium none;
    font-size: 15px;
    font-family: BYekan;
    border-radius: 5px;
    box-shadow: 0px 0px 5px #333 inset;
  }
  input:focus {
    box-shadow: 0px 0px 5px #43ab92 inset;
  }
`
const AddUserSelectContainer = styled.div`
  width: 150px;
  height: 36px;
  margin-right: 10px;
  border-radius: 5px;
  select {
    width: 100%;
    height: 100%;
  }
`
const AddUserBTN = styled.button`
  width: 100px;
  height: 36px;
  display: block;
  background: #43ab92;
  border-radius: 5px;
  border: medium none;
  text-align: center;
  line-height: 36px;
  color: #fff;
  font-family: BYekan;
  float: right;
  margin-top: 20px;
  margin-right: 10px;
  cursor: pointer;
`
const LoginMsgBox = styled.div`
  width: 350px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 40px auto;
  text-align: center;
  line-height: 40px;
  color: #fff;
  border-radius: 5px;
`
